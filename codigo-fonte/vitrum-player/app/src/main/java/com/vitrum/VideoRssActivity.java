package com.vitrum;

import android.os.Bundle;

/**
 * Classe responsável por controlar a activity do tipo Video e Rss.
 *
 * @author DavidBen
 */
public class VideoRssActivity extends VitrumActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_rss);
    }
}
