package com.vitrum.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Classe utilitária para auxiliar no uso de objetos String.
 *
 * @author DavidBen
 */
public final class StringUtil {

    public static final int SERIALKEY_LENGTH = 12;
    private static final char[] SERIALKEY_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();

    public static final String trimAll(final String string) {
        return string.replaceAll(" ", "");
    }

    public static final boolean isValid(final String string) {
        return string != null && !string.isEmpty();
    }

    public static final String checkSum(final String string) {
        String md5String = null;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(string.getBytes());
            byte bmessageDigest[] = messageDigest.digest();

            for (byte aMessageDigest : bmessageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) stringBuilder.append('0');
                stringBuilder.append(hex);
            }
            md5String = stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5String;
    }

    //Irá adicionar hífen a cada 4 caracteres.
    public static final String generateSerialKey(int length) {
        String key;
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        int index;

        for (int i = 1; i <= length; i++) {
            index = random.nextInt(SERIALKEY_CHARS.length);
            stringBuilder.append(SERIALKEY_CHARS[index]);
            if (i % (length / 4) == 0)
                stringBuilder.append("-");
        }
        key = stringBuilder.toString();
        key = key.substring(0, key.length() - 1);

        return key;
    }

}
