package com.vitrum;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.vitrum.entity.Frame;
import com.vitrum.model.Model;

/**
 * Classe responsável definir o comportamento padrão das activitys.
 *
 * @author DavidBen
 */
public class VitrumActivity extends FragmentActivity {

    private static final String TAG_LOG = VitrumActivity.class.getName();

    private Model model;

    protected VitrumActivity() {
        model = Model.getInstance();
    }

    public void onNextActivity() {
        Intent intent = null;

        model.nextFrame();

        Log.i(TAG_LOG, "Executando " + model.getFrame() + " tipo " + model.getFrame().getType());

        switch (model.getFrame().getType()) {
            case Frame.TYPE_VIDEO_IMAGE_RSS:
                intent = new Intent(this, VideoImageRssActivity.class);
                break;
            case Frame.TYPE_VIDEO_IMAGE:
                intent = new Intent(this, VideoImageActivity.class);
                break;
            case Frame.TYPE_VIDEO_RSS:
                intent = new Intent(this, VideoRssActivity.class);
                break;
            case Frame.TYPE_VIDEO:
                intent = new Intent(this, VideoActivity.class);
                break;
            case Frame.TYPE_IMAGE_RSS:
                intent = new Intent(this, ImageRssActivity.class);
                break;
            case Frame.TYPE_IMAGE:
                intent = new Intent(this, ImageActivity.class);
                break;
            default:
                break;
        }
        startActivity(intent);
        onNextTransitionAnimation();
    }

    private void onNextTransitionAnimation() {
        //overridePendingTransition(R.anim.fade, R.anim.fade);
    }

    protected Model getModel() {
        return model;
    }
}
