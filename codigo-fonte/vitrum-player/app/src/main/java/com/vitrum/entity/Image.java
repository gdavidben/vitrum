package com.vitrum.entity;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.vitrum.util.FileUtil;

import java.io.File;

/**
 * Entidade Image.
 *
 * @author DavidBen
 */
public class Image {

    private Long id;
    private String format;
    private String animation;
    private Long duration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getAnimation() {
        return animation;
    }

    public void setAnimation(String animation) {
        this.animation = animation;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public File getFile(Activity activity) {
        File file = FileUtil.loadExternalFile(activity, FileUtil.DIRECTORY_IMAGES, this.getId().toString() + "." + this.getFormat());
        if (!file.exists())
            file = null;
        return file;
    }

    public Drawable getDrawable(Activity activity) {
        Drawable drawable = BitmapDrawable.createFromPath(getFile(activity).getAbsolutePath());
        return drawable;
    }
}
