package com.vitrum.handler;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import com.vitrum.model.Model;
import com.vitrum.task.SyncTask;

/**
 * Classe responsável por iniciar a tarefa de sincronismo.
 *
 * @author DavidBen
 */
public class SyncHandler extends Handler implements Runnable {

    private static final String TAG_LOG = SyncHandler.class.getName();

    private Model model;
    private SyncTask syncTask;
    private Activity activity;

    public SyncHandler(Activity activity) {
        this.model = Model.getInstance();
        this.syncTask = new SyncTask();
        this.activity = activity;
    }

    @Override
    public void run() {
        Log.i(TAG_LOG, "Executando sincronismo.");
        this.syncTask.execute(activity, this);
    }

}
