package com.vitrum.fragment;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.vitrum.R;
import com.vitrum.entity.Image;

import java.util.LinkedList;

/**
 * Classe responsável por controlar o fragmento do tipo Image.
 *
 * @author DavidBen
 */
public class ImageFragment extends VitrumFragment {

    private static final String TAG_LOG = ImageFragment.class.getName();

    private ImageView imageView;
    private Handler handler;
    private Runnable runnable;
    BitmapDrawable bitmapDrawable = null;
    private AnimationDrawable animationDrawable;
    private Image image;

    public ImageFragment() {
        super();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                onNextResource();
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        onLoadFragment(inflater, container);
        onStartFragment();
        return getBaseView();
    }

    @Override
    protected void onLoadFragment(LayoutInflater inflater, ViewGroup container) {
        setBaseView(inflater.inflate(R.layout.fragment_image, container, false));
        setQueue(new LinkedList<Image>(getModel().getFrame().getImages()));
        imageView = (ImageView) getBaseView().findViewById(R.id.imageView);
    }

    @Override
    protected void onStartFragment() {
        onNextResource();
    }

    @Override
    protected void onNextResource() {
        if (isValidQueue()) {

            Long duration = new Long(0L);
            animationDrawable = new AnimationDrawable();

            while ((image = (Image) getQueue().poll()) != null) {
                bitmapDrawable = (BitmapDrawable) image.getDrawable(getActivity());
                animationDrawable.addFrame(bitmapDrawable, image.getDuration().intValue());
                duration += image.getDuration();
                Log.d(TAG_LOG, "Preparando para visualizar imagem " + image.getId() + "." + image.getFormat() + " [" + image.getDuration() + "ms]");
            }
            if (animationDrawable.getNumberOfFrames() > 0) {
                imageView.setImageDrawable(animationDrawable);
                imageView.postDelayed(runnable, duration);
                animationDrawable.start();
            } else {
                //Os vídeos são mandatários, eles que controlarão o tempo de exibição.
                if (getModel().getFrame().getVideos() != null && !getModel().getFrame().getVideos().isEmpty()) {
                    setQueue(new LinkedList<Image>(getModel().getFrame().getImages()));
                    onNextResource();
                } else {
                    onNextFrame();
                }
            }

        } else {
            //Os vídeos são mandatários, eles que controlarão o tempo de exibição.
            if (getModel().getFrame().getVideos() != null && !getModel().getFrame().getVideos().isEmpty()) {
                setQueue(new LinkedList<Image>(getModel().getFrame().getImages()));
                onNextResource();
            } else {
                onNextFrame();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imageView.setImageDrawable(null);
        animationDrawable = null;
        if (bitmapDrawable != null && !bitmapDrawable.getBitmap().isRecycled()) {
            bitmapDrawable.getBitmap().recycle();
            bitmapDrawable = null;
        }
    }
}
