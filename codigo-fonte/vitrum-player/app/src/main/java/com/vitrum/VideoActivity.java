package com.vitrum;

import android.os.Bundle;

/**
 * Classe responsável por controlar a activity do tipo Video.
 *
 * @author DavidBen
 */
public class VideoActivity extends VitrumActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
    }

}
