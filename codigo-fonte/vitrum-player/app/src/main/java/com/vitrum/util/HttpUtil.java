package com.vitrum.util;

import android.util.Base64;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe utilitária para auxiliar nas requisições HTTP.
 *
 * @author DavidBen
 */
public final class HttpUtil {

    private static final String TAG_LOG = HttpUtil.class.getName();

    private static final int HTTP_TIMEOUT = 1000 * 30;
    private static final String HTTP_METHOD_GET = "GET";
    private static final String HTTP_METHOD_POST = "POST";

    private static final HttpURLConnection prepareConnection(String stringUrl, String method) {
        HttpURLConnection httpURLConnection = null;
        String userPass = "david:david";
        String basicAuth = "Basic " + new String(Base64.encode(userPass.getBytes(), Base64.DEFAULT));
        try {
            URL url = new URL(stringUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(HTTP_TIMEOUT);
            httpURLConnection.setConnectTimeout(HTTP_TIMEOUT);
            httpURLConnection.setRequestMethod(method);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("charset", "UTF-8");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestProperty("Authorization", basicAuth);
            //httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        } catch (MalformedURLException e) {
            Log.e(TAG_LOG, "URL não encontrada.");
        } catch (IOException e) {
            Log.e(TAG_LOG, "Erro ao abrir conexão com: " + stringUrl);
        }
        return httpURLConnection;
    }

    private static final String prepareParams(HashMap<String, Object> hmParams) {
        StringBuilder stringBuilder = new StringBuilder();
        String result = null;
        String key = null;
        Object value = null;
        boolean first = true;

        for (Map.Entry<String, Object> entry : hmParams.entrySet()) {
            key = entry.getKey();
            value = entry.getValue();

            if (first)
                first = false;
            else
                stringBuilder.append("&");

            stringBuilder.append(key);
            stringBuilder.append("=");
            stringBuilder.append(value);

            try {
                result = URLEncoder.encode(stringBuilder.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG_LOG, "Erro ao preparar parâmetros.");
            }
        }
        return result;
    }

    private static final String request(String stringUrl, HashMap<String, Object> hmParams, String method) {
        String response = null;
        HttpURLConnection httpURLConnection = prepareConnection(stringUrl, method);
        InputStream inputStream = null;

        String params = prepareParams(hmParams);

        try {
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(params.getBytes("UTF-8"));

            //BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            //writer.write(getQuery(params));
            //writer.flush();
            //writer.close();
            //os.close();

            httpURLConnection.connect();

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED) {
                inputStream = httpURLConnection.getInputStream();
                response = FileUtil.inputStreamToString(inputStream);
            }

            Log.i(TAG_LOG, "Enviando requisição " + method + " para a URL " + stringUrl);
            Log.i(TAG_LOG, "Parâmetros: " + params);
            Log.i(TAG_LOG, "Código de retorno: " + httpURLConnection.getResponseCode());
        } catch (IOException e) {
            Log.e(TAG_LOG, "Erro ao conectar com: " + stringUrl);
        }
        return response;
    }

    public static final String get(String stringUrl, HashMap<String, Object> hmParams) {
        return request(stringUrl, hmParams, HTTP_METHOD_GET);
    }

    public static final String post(String stringUrl, HashMap<String, Object> hmParams) {
        return request(stringUrl, hmParams, HTTP_METHOD_POST);
    }
}
