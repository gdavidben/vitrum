package com.vitrum;

import android.os.Bundle;

/**
 * Classe responsável por controlar a activity do tipo Video, Image e Rss.
 *
 * @author DavidBen
 */
public class VideoImageRssActivity extends VitrumActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_image_rss);
    }

}
