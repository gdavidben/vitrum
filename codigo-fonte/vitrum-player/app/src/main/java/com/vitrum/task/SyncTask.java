package com.vitrum.task;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.vitrum.ImageActivity;
import com.vitrum.ImageRssActivity;
import com.vitrum.MainActivity;
import com.vitrum.R;
import com.vitrum.VideoActivity;
import com.vitrum.VideoImageActivity;
import com.vitrum.VideoImageRssActivity;
import com.vitrum.VideoRssActivity;
import com.vitrum.entity.Config;
import com.vitrum.entity.Frame;
import com.vitrum.handler.SyncHandler;
import com.vitrum.model.Model;
import com.vitrum.util.DateUtil;
import com.vitrum.util.FileUtil;
import com.vitrum.util.HttpUtil;
import com.vitrum.util.JsonUtil;
import com.vitrum.util.StringUtil;

import java.util.HashMap;

/**
 * Classe responsável por definir o comportamento de sicronização da aplicação.
 *
 * @author DavidBen
 */
public class SyncTask extends AsyncTask {

    private static final String TAG_LOG = SyncTask.class.getName();

    private Activity activity;
    private Model model;
    private SyncHandler syncHandler;

    @Override
    protected Object doInBackground(Object[] params) {
        activity = (Activity) params[0];
        syncHandler = (SyncHandler) params[1];
        model = Model.getInstance();

        return load();
    }

    private Config load() {
        Config config = loadConfig();
        //config = syncConfigServer(config);
        startActivity(config);

        return config;
    }

    private Config loadConfig() {
        Config config;
        String contentConfig = FileUtil.loadExternalFileString(activity, FileUtil.DIRECTORY_CONFIGS, FileUtil.FILE_CONFIG);

        //Primeira configuração, deve gerar o player_id.
        if (!StringUtil.isValid(contentConfig)) {
            contentConfig = FileUtil.loadInternalFileString(activity, R.raw.config_default);
            config = JsonUtil.jsonTOconfig(contentConfig);
            config.setSerialKey(StringUtil.generateSerialKey(StringUtil.SERIALKEY_LENGTH));
            showSerialKeyActivity(config);
        } else
            config = JsonUtil.jsonTOconfig(contentConfig);
        return config;
    }

    private void showSerialKeyActivity(Config config) {
        TextView tv = (TextView) activity.findViewById(R.id.tvSerialkey);
        tv.setText(config.getSerialKey());
        activity.startActivity(new Intent(activity, MainActivity.class));
    }

    private Config syncConfigServer(Config config) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("serialKey", config.getSerialKey());
        params.put("dateLastUpdate", DateUtil.dateTOstring(config.getDateLastUpdate()));

        String response = HttpUtil.post(config.getUrlCheckConfig(), params);

        if (StringUtil.isValid(response)) {
            FileUtil.createExternalFile(activity, FileUtil.DIRECTORY_CONFIGS, FileUtil.FILE_CONFIG, response);
            String contentConfig = FileUtil.loadExternalFileString(activity, FileUtil.DIRECTORY_CONFIGS, FileUtil.FILE_CONFIG);
            config = JsonUtil.jsonTOconfig(contentConfig);
        }
        return config;
    }

    private void startActivity(Config config) {
        if (config != null && config.getFrames() != null && !config.getFrames().isEmpty()) {
            model.clearQueueFrame();
            model.addQueueFrame(config.getFrames());

            if (model.getFrame() == null) {
                onNextActivity();
            }
        }
    }

    private void onNextActivity() {
        Intent intent = null;

        model.nextFrame();

        Log.i(TAG_LOG, "Executando " + model.getFrame() + " tipo " + model.getFrame().getType());

        switch (model.getFrame().getType()) {
            case Frame.TYPE_VIDEO_IMAGE_RSS:
                intent = new Intent(activity, VideoImageRssActivity.class);
                break;
            case Frame.TYPE_VIDEO_IMAGE:
                intent = new Intent(activity, VideoImageActivity.class);
                break;
            case Frame.TYPE_VIDEO_RSS:
                intent = new Intent(activity, VideoRssActivity.class);
                break;
            case Frame.TYPE_VIDEO:
                intent = new Intent(activity, VideoActivity.class);
                break;
            case Frame.TYPE_IMAGE_RSS:
                intent = new Intent(activity, ImageRssActivity.class);
                break;
            case Frame.TYPE_IMAGE:
                intent = new Intent(activity, ImageActivity.class);
                break;
            default:
                break;
        }
        activity.startActivity(intent);
        onSelectNextTransitionAnimation();
    }

    private void onSelectNextTransitionAnimation() {
        //overridePendingTransition(R.anim.fade, R.anim.fade);
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        model.setConfig((Config) o);
        Log.i(TAG_LOG, "Finalizando sincronismo e agendando próxima tarefa [" + model.getTimeCheckConfig() + "ms] [" + model.getConfig() + "].");
        syncHandler.postDelayed(new SyncHandler(activity), model.getTimeCheckConfig());
    }

}