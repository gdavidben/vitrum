package com.vitrum.fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.vitrum.R;
import com.vitrum.entity.Video;

import java.io.File;
import java.util.LinkedList;

/**
 * Classe responsável por controlar o fragmento do tipo Video.
 *
 * @author DavidBen
 */
public class VideoFragment extends VitrumFragment {

    private static final String TAG_LOG = VideoFragment.class.getName();
    private VideoView videoView;

    public VideoFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        onLoadFragment(inflater, container);
        onStartFragment();
        return getBaseView();
    }

    @Override
    protected void onLoadFragment(LayoutInflater inflater, ViewGroup container) {
        setBaseView(inflater.inflate(R.layout.fragment_video, container, false));
        setQueue(new LinkedList<Video>(getModel().getFrame().getVideos()));
        videoView = (VideoView) getBaseView().findViewById(R.id.videoView);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                onStartFragment();
            }
        });
    }

    @Override
    protected void onStartFragment() {
        onNextResource();
    }

    @Override
    protected void onNextResource() {
        Video video = (Video) getQueue().poll();
        if (video != null) {
            File file = video.getFile(getActivity());
            if (file != null) {
                Log.d(TAG_LOG, "Visualizando video " + file.getName());
                videoView.setVideoPath(file.getAbsolutePath());
                videoView.start();
            } else {
                if (isValidQueue()) {
                    onNextResource();
                } else {
                    onNextFrame();
                }
            }
        } else {
            if (isValidQueue()) {
                onNextResource();
            } else {
                onNextFrame();
            }
        }
    }
}
