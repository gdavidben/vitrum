package com.vitrum;

import android.os.Bundle;

/**
 * Classe responsável por controlar a activity do tipo Video e Image.
 *
 * @author DavidBen
 */
public class VideoImageActivity extends VitrumActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_image);
    }

}
