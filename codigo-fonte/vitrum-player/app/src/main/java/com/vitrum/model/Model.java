package com.vitrum.model;

import com.vitrum.entity.Config;
import com.vitrum.entity.Frame;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Classe responsável por armazenar o estado atual da aplicação.
 *
 * @author DavidBen
 */
public class Model {

    private static final String TAG_LOG = Model.class.getName();
    private static Model model;

    private Config config;
    private Queue<Frame> queueFrames;
    private Queue<Frame> queueFramesClone;
    private Frame frame;

    private Model() {
        queueFrames = new LinkedList<>();
        queueFramesClone = new LinkedList<>();
    }

    public static synchronized Model getInstance() {
        if (model == null)
            model = new Model();
        return model;
    }

    public void addQueueFrame(Queue<Frame> frames) {
        if (frames != null) {
            queueFrames.addAll(frames);
            queueFramesClone.addAll(frames);
        }
    }

    public void clearQueueFrame() {
        queueFrames.clear();
    }

    public void nextFrame() {
        frame = (Frame) queueFrames.poll();
        if (frame == null) {
            queueFrames.addAll(queueFramesClone);
            nextFrame();
        }
    }

    public void setConfig(Config c) {
        config = c;
    }

    public Config getConfig() {
        return config;
    }

    public Frame getFrame() {
        return frame;
    }

    public Long getTimeCheckConfig() {
        Long timeCheckConfig = Config.MINIMAL_TIME_CHECK_CONFIG;
        if (config != null && config.getTimeCheckConfig() >= Config.MINIMAL_TIME_CHECK_CONFIG)
            timeCheckConfig = config.getTimeCheckConfig();
        return timeCheckConfig;
    }
}
