package com.vitrum.fragment;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vitrum.VitrumActivity;
import com.vitrum.model.Model;

import java.util.Queue;

/**
 * Classe responsável definir o comportamento padrão dos fragmentos.
 *
 * @author DavidBen
 */
public abstract class VitrumFragment extends Fragment {

    private static final String TAG_LOG = VitrumFragment.class.getName();

    private Model model;
    private View baseView;
    private Queue queue;

    protected VitrumFragment() {
        model = Model.getInstance();
    }

    protected abstract void onLoadFragment(LayoutInflater inflater, ViewGroup container);

    protected abstract void onStartFragment();

    protected abstract void onNextResource();

    protected void onNextFrame() {
        ((VitrumActivity) getActivity()).onNextActivity();
    }

    protected boolean isValidQueue() {
        return queue != null && !queue.isEmpty();
    }

    protected Model getModel() {
        return model;
    }

    protected View getBaseView() {
        return baseView;
    }

    protected void setBaseView(View baseView) {
        this.baseView = baseView;
    }

    protected Queue getQueue() {
        return queue;
    }

    protected void setQueue(Queue queue) {
        this.queue = queue;
    }
}
