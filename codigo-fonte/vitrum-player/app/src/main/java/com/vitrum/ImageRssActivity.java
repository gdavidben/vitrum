package com.vitrum;

import android.os.Bundle;

/**
 * Classe responsável por controlar a activity do tipo Image e Rss.
 *
 * @author DavidBen
 */
public class ImageRssActivity extends VitrumActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_rss);
    }

}
