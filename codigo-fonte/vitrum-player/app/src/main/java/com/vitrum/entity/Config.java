package com.vitrum.entity;

import java.util.Date;
import java.util.Queue;

/**
 * Entidade de Config.
 *
 * @author DavidBen
 */
public class Config {

    public static final Long MINIMAL_TIME_CHECK_CONFIG = new Long(1000 * 60);

    private String urlCheckConfig;
    private String urlDownloadFile;
    private String serialKey;
    private Long timeCheckConfig;
    private Date dateLastUpdate;
    private Queue<Frame> frames;

    public String getUrlCheckConfig() {
        return urlCheckConfig;
    }

    public void setUrlCheckConfig(String urlCheckConfig) {
        this.urlCheckConfig = urlCheckConfig;
    }

    public String getUrlDownloadFile() {
        return urlDownloadFile;
    }

    public void setUrlDownloadFile(String urlDownloadFile) {
        this.urlDownloadFile = urlDownloadFile;
    }

    public String getSerialKey() {
        return serialKey;
    }

    public void setSerialKey(String serialKey) {
        this.serialKey = serialKey;
    }

    public Long getTimeCheckConfig() {
        return timeCheckConfig;
    }

    public void setTimeCheckConfig(Long timeCheckConfig) {
        this.timeCheckConfig = timeCheckConfig;
    }

    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public Queue<Frame> getFrames() {
        return frames;
    }

    public void setFrames(Queue<Frame> frames) {
        this.frames = frames;
    }
}
