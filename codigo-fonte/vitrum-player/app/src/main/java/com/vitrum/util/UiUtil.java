package com.vitrum.util;

import android.app.Activity;
import android.view.View;

/**
 * Classe utilitária para auxiliar na configuração da interface.
 *
 * @author DavidBen
 */
public final class UiUtil {

    private static final int FLAGS_FOR_FULL_SCREEN = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

    public static void enableFullScreenMode(Activity activity) {
        activity.getWindow().getDecorView().setSystemUiVisibility(FLAGS_FOR_FULL_SCREEN);
    }

}
