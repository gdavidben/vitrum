package com.vitrum.util;

import com.vitrum.entity.Config;
import com.vitrum.entity.Frame;
import com.vitrum.entity.Image;
import com.vitrum.entity.Video;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Classe utilitária para auxiliar na transformação de objetos Json.
 *
 * @author DavidBen
 */
public final class JsonUtil {

    private static final String TAG_CONFIG = "config";
    private static final String TAG_URL_CHECK_CONFIG = "urlCheckConfig";
    private static final String TAG_URL_DOWNLOAD_FILE = "urlDownloadFile";
    private static final String TAG_TIME_CHECK_CONFIG = "timeCheckConfig";
    private static final String TAG_DATE_LAST_UPDATE = "dateLastUpdate";
    private static final String TAG_SERIAL_KEY = "serialKey";
    private static final String TAG_URL_RSS = "urlRss";
    private static final String TAG_FRAMES = "frames";
    private static final String TAG_VIDEOS = "videos";
    private static final String TAG_IMAGES = "images";
    private static final String TAG_ANIMATION = "animation";
    private static final String TAG_DURATION = "duration";
    private static final String TAG_ID = "id";
    private static final String TAG_TYPE = "type";
    private static final String TAG_FORMAT = "format";

    public static final Config jsonTOconfig(String json) {
        Config config = new Config();

        if (json != null) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                JSONObject jsonConfig = jsonObject.getJSONObject(TAG_CONFIG);
                JSONArray jsonFrames = null;

                config.setUrlCheckConfig(jsonConfig.getString(TAG_URL_CHECK_CONFIG));
                config.setUrlDownloadFile(jsonConfig.getString(TAG_URL_DOWNLOAD_FILE));
                config.setTimeCheckConfig(jsonConfig.getLong(TAG_TIME_CHECK_CONFIG));

                if (!jsonConfig.isNull(TAG_SERIAL_KEY))
                    config.setSerialKey(jsonConfig.getString(TAG_SERIAL_KEY));

                if (!jsonConfig.isNull(TAG_DATE_LAST_UPDATE))
                    config.setDateLastUpdate(DateUtil.stringTOdate(jsonConfig.getString(TAG_DATE_LAST_UPDATE)));

                if (!jsonConfig.isNull(TAG_FRAMES))
                    jsonFrames = jsonConfig.getJSONArray(TAG_FRAMES);

                if (jsonFrames != null) {
                    config.setFrames(new LinkedList<Frame>());
                    JSONObject jsonFrame = null;
                    Frame frame = null;
                    Video video = null;
                    Image image = null;

                    for (int fi = 0; fi < jsonFrames.length(); fi++) {
                        jsonFrame = (JSONObject) jsonFrames.get(fi);
                        JSONArray jsonImages = null;
                        JSONArray jsonVideos = null;

                        if (!jsonFrame.isNull(TAG_VIDEOS))
                            jsonVideos = jsonFrame.getJSONArray(TAG_VIDEOS);
                        if (!jsonFrame.isNull(TAG_IMAGES))
                            jsonImages = jsonFrame.getJSONArray(TAG_IMAGES);

                        frame = new Frame();
                        frame.setType(jsonFrame.getString(TAG_TYPE));

                        if (!jsonFrame.isNull(TAG_ANIMATION))
                            frame.setAnimation(jsonFrame.getString(TAG_ANIMATION));
                        if (!jsonFrame.isNull(TAG_URL_RSS))
                            frame.setUrlRss(jsonFrame.getString(TAG_URL_RSS));

                        if (jsonVideos != null) {
                            frame.setVideos(new LinkedList<Video>());
                            JSONObject jsonVideo = null;

                            for (int vi = 0; vi < jsonVideos.length(); vi++) {
                                jsonVideo = (JSONObject) jsonVideos.get(vi);
                                video = new Video();
                                video.setId(jsonVideo.getLong(TAG_ID));
                                video.setFormat(jsonVideo.getString(TAG_FORMAT));
                                frame.getVideos().add(video);
                            }
                        }

                        if (jsonImages != null) {
                            frame.setImages(new LinkedList<Image>());
                            JSONObject jsonImage = null;

                            for (int ii = 0; ii < jsonImages.length(); ii++) {
                                jsonImage = (JSONObject) jsonImages.get(ii);
                                image = new Image();
                                image.setId(jsonImage.getLong(TAG_ID));
                                image.setFormat(jsonImage.getString(TAG_FORMAT));
                                image.setDuration(jsonImage.getLong(TAG_DURATION));
                                if (!jsonImage.isNull(TAG_ANIMATION))
                                    image.setAnimation(jsonImage.getString(TAG_ANIMATION));
                                frame.getImages().add(image);
                            }
                        }
                        config.getFrames().add(frame);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return config;
    }

    public static String configTOjson(Config config) {
        String json = null;

        try {
            if (config != null) {
                JSONObject jsonObject = new JSONObject();
                JSONObject jsonConfig = new JSONObject();

                jsonConfig.put(TAG_URL_CHECK_CONFIG, config.getUrlCheckConfig());
                jsonConfig.put(TAG_URL_DOWNLOAD_FILE, config.getUrlDownloadFile());
                jsonConfig.put(TAG_TIME_CHECK_CONFIG, config.getTimeCheckConfig());
                jsonConfig.put(TAG_SERIAL_KEY, config.getSerialKey());

                if (config.getDateLastUpdate() != null)
                    jsonConfig.put(TAG_DATE_LAST_UPDATE, DateUtil.dateTOstring(config.getDateLastUpdate()));

                if (config.getFrames() != null && !config.getFrames().isEmpty()) {
                    JSONArray jsonFrames = new JSONArray();
                    JSONArray jsonVideos = null;
                    JSONArray jsonImages = null;
                    JSONObject jsonFrame = null;
                    JSONObject jsonVideo = null;
                    JSONObject jsonImage = null;

                    for (Frame frame : config.getFrames()) {
                        jsonFrame = new JSONObject();
                        jsonFrame.put(TAG_TYPE, frame.getType());
                        if (StringUtil.isValid(frame.getAnimation()))
                            jsonFrame.put(TAG_ANIMATION, frame.getAnimation());
                        if (StringUtil.isValid(frame.getUrlRss()))
                            jsonFrame.put(TAG_URL_RSS, frame.getUrlRss());

                        if (frame.getVideos() != null && !frame.getVideos().isEmpty()) {
                            jsonVideos = new JSONArray();

                            for (Video video : frame.getVideos()) {
                                jsonVideo = new JSONObject();
                                jsonVideo.put(TAG_ID, video.getId());
                                jsonVideo.put(TAG_FORMAT, video.getFormat());
                                jsonVideos.put(jsonVideo);
                            }
                            jsonFrame.put(TAG_VIDEOS, jsonVideos);
                        }

                        if (frame.getImages() != null && !frame.getImages().isEmpty()) {
                            jsonImages = new JSONArray();

                            for (Image image : frame.getImages()) {
                                jsonImage = new JSONObject();
                                jsonImage.put(TAG_ID, image.getId());
                                jsonImage.put(TAG_FORMAT, image.getFormat());
                                jsonImage.put(TAG_DURATION, image.getDuration());
                                if (StringUtil.isValid(image.getAnimation()))
                                    jsonImage.put(TAG_ANIMATION, image.getAnimation());
                                jsonImages.put(jsonImage);
                            }
                            jsonFrame.put(TAG_IMAGES, jsonImages);
                        }
                        jsonFrames.put(jsonFrame);
                    }
                    jsonConfig.put(TAG_FRAMES, jsonFrames);
                }

                jsonObject.put(TAG_CONFIG, jsonConfig);

                json = jsonObject.toString();
                json = json.replaceAll("\\\\", "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

}
