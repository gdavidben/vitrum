package com.vitrum.util;

import android.app.Activity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Classe utilitária para auxiliar na leitura e escrita de arquivos.
 *
 * @author DavidBen
 */
public final class FileUtil {

    private static final String TAG_LOG = FileUtil.class.getName();

    public static final String DIRECTORY_VIDEOS = "videos/";
    public static final String DIRECTORY_IMAGES = "images/";
    public static final String DIRECTORY_CONFIGS = "configs/";

    public static final String FILE_CONFIG = "config.json";
    public static final String FILE_CONFIG_DEFAULT = "config_default.json";

    public static final File createExternalFile(Activity activity, String path, String fileName, String fileContent) {
        File file = null;
        try {
            file = loadExternalFile(activity, path, fileName);

            if (fileContent != null) {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
                outputStreamWriter.write(fileContent);
                outputStreamWriter.close();
            }

            if (exists(file) && file.isFile()) {
                file = loadExternalFile(activity, path, fileName);
            } else {
                Log.d(TAG_LOG, "Impossível carregar o arquivo: " + file.getPath());
            }

        } catch (IOException e) {
            e.getStackTrace();
        }
        return file;
    }

    public static final File loadExternalFile(Activity activity, String path, String fileName) {
        return new File(activity.getExternalFilesDir(path), fileName);
    }

    private static final InputStream loadExternalFileInputStream(Activity activity, String path, String fileName) {
        File file = null;
        FileInputStream fileInputStream = null;

        try {
            file = loadExternalFile(activity, path, fileName);

            if (file != null) {
                fileInputStream = new FileInputStream(file);
            }
        } catch (FileNotFoundException e) {
            Log.i(TAG_LOG, "Arquivo não encontrado: " + file.getPath());
        }
        return fileInputStream;
    }

    public static final String loadExternalFileString(Activity activity, String path, String fileName) {
        InputStream inputStream = loadExternalFileInputStream(activity, path, fileName);
        return inputStreamToString(inputStream);
    }

    private static final InputStream loadInternalFileInputStream(Activity activity, int fileId) {
        return activity.getResources().openRawResource(fileId);
    }

    public static final String loadInternalFileString(Activity activity, int fileId) {
        InputStream inputStream = loadInternalFileInputStream(activity, fileId);
        return inputStreamToString(inputStream);
    }

    public static final boolean exists(File file) {
        if (file != null && file.exists()) {
            return true;
        }
        return false;
    }

    public static final String inputStreamToString(InputStream inputStream) {
        String line = null;
        String string = null;
        StringBuilder stringBuilder = null;

        try {
            if (inputStream != null) {
                stringBuilder = new StringBuilder();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                inputStream.close();
                string = stringBuilder.toString();
            }
        } catch (IOException e) {
            Log.e(TAG_LOG, "Erro ao ler inputStream!");
        }
        return string;
    }
}
