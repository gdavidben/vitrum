package com.vitrum.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe utilitária para auxiliar no uso de objetos do tipo Date.
 *
 * @author DavidBen
 */
public final class DateUtil {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static SimpleDateFormat simpleDateFormat;

    private static synchronized SimpleDateFormat getSimpleDateFormat() {
        if (simpleDateFormat == null)
            simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        return simpleDateFormat;
    }

    public static final Date stringTOdate(String stringDate) {
        Date date = null;
        try {
            date = getSimpleDateFormat().parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static final String dateTOstring(Date date) {
        return simpleDateFormat.format(date);
    }

}
