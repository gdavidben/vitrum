package com.vitrum.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vitrum.R;
import com.vitrum.entity.Rss;

import java.util.LinkedList;

/**
 * Classe responsável por controlar o fragmento do tipo Rss.
 *
 * @author DavidBen
 */
public class RssFragment extends VitrumFragment {

    private static final String TAG_LOG = ImageFragment.class.getName();

    private TextView textView;

    public RssFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        onLoadFragment(inflater, container);
        onStartFragment();
        return getBaseView();
    }

    @Override
    protected void onLoadFragment(LayoutInflater inflater, ViewGroup container) {
        setBaseView(inflater.inflate(R.layout.fragment_image, container, false));
        setQueue(new LinkedList<Rss>(getModel().getFrame().getRsss()));
        textView = (TextView) getBaseView().findViewById(R.id.textView);
    }

    @Override
    protected void onStartFragment() {
        onNextResource();
    }

    @Override
    protected void onNextResource() {
        Rss rss = (Rss) getQueue().poll();
        if (rss != null) {
            Log.d(TAG_LOG, "Visualizando rss " + rss.getTitle() + " - " + rss.getDescription());
            textView.setText(rss.getTitle() + " - " + rss.getDescription());
        } else {
            if (isValidQueue()) {
                onNextResource();
            } else {
                onNextFrame();
            }
        }
    }

}
