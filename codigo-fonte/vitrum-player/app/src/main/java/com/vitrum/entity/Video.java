package com.vitrum.entity;

import android.app.Activity;

import com.vitrum.util.FileUtil;

import java.io.File;

/**
 * Entidade Video.
 *
 * @author DavidBen
 */
public class Video {

    private Long id;
    private String format;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public File getFile(Activity activity) {
        File file = FileUtil.loadExternalFile(activity, FileUtil.DIRECTORY_VIDEOS, this.getId().toString() + "." + this.getFormat());
        if (!file.exists())
            file = null;
        return file;
    }
}
