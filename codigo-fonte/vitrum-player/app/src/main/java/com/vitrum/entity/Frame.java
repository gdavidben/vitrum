package com.vitrum.entity;

import java.util.Queue;

/**
 * Entidade Frame.
 *
 * @author DavidBen
 */
public class Frame {

    public static final String TYPE_VIDEO_IMAGE_RSS = "VIDEO_IMAGE_RSS";
    public static final String TYPE_VIDEO_IMAGE = "VIDEO_IMAGE";
    public static final String TYPE_VIDEO_RSS = "VIDEO_RSS";
    public static final String TYPE_VIDEO = "VIDEO";
    public static final String TYPE_IMAGE_RSS = "IMAGE_RSS";
    public static final String TYPE_IMAGE = "IMAGE";
    public static final String ANIMATION_FADE = "FADE";

    private String type;
    private Queue<Video> videos;
    private Queue<Image> images;
    private String urlRss;
    private Queue<Rss> rsss;
    private String animation;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Queue<Video> getVideos() {
        return videos;
    }

    public void setVideos(Queue<Video> videos) {
        this.videos = videos;
    }

    public Queue<Image> getImages() {
        return images;
    }

    public void setImages(Queue<Image> images) {
        this.images = images;
    }

    public String getUrlRss() {
        return urlRss;
    }

    public void setUrlRss(String urlRss) {
        this.urlRss = urlRss;
    }

    public Queue<Rss> getRsss() {
        return rsss;
    }

    public void setRsss(Queue<Rss> rsss) {
        this.rsss = rsss;
    }

    public String getAnimation() {
        return animation;
    }

    public void setAnimation(String animation) {
        this.animation = animation;
    }
}
