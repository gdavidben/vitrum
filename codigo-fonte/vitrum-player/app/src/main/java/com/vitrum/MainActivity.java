package com.vitrum;

import android.app.Activity;
import android.os.Bundle;

import com.vitrum.handler.SyncHandler;
import com.vitrum.util.UiUtil;

/**
 * Classe Main da aplicação.
 *
 * @author DavidBen
 */
public class MainActivity extends Activity {

    public MainActivity() {
        new SyncHandler(this).run();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        UiUtil.enableFullScreenMode(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}