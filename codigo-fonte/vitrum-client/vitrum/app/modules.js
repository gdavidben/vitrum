(function () {
    'use strict';
    angular.module('dng', []);
    
    angular.module('vitrum', [
        'ui.router',
        'ngResource',
        'ngSanitize',
        'ngAnimate',
        'ngToast',
        'ngCookies',
        'dng',
        'vcRecaptcha',
        'ngMaterial'
    ]);
})();