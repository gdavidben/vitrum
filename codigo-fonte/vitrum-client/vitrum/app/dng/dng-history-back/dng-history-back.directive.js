(function () {
    'use strict';
    angular
            .module('dng')
            .directive('dngHistoryBack', DngHistoryBackDirective);

    DngHistoryBackDirective.$inject = ['$window'];
    function DngHistoryBackDirective($window) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.bind('click', function () {
                    $window.history.back();
                });
            }
        };
    }

})();