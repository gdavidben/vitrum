(function () {
    'use strict';
    angular
            .module('dng')
            .directive('dngPanel', DngPanelDirective);

    DngPanelDirective.$inject = [];
    function DngPanelDirective() {
        return {
            restrict: 'E',
            transclude: true,
            replace: true, //Substitui a tag <util-panel> pelo template
            templateUrl: 'dng/dng-panel/dng-panel.html',
            scope: {
                dngTitle: '@dngTitle', //= two binding, @ one binding
                dngCollapse: '@dngCollapse'
            },
            compile: function(element, attrs) {
                attrs.dngCollapse = (attrs.dngCollapse === undefined) ? 'true' : attrs.dngCollapse;
            }
        };
    }

})();