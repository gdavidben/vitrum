(function () {
    'use strict';
    angular
            .module('dng')
            .directive('dngTooltip', DngTooltipDirective);

    DngTooltipDirective.$inject = [];
    function DngTooltipDirective() {
        return {
            restrict: 'E',
            transclude: true,
            replace: true, //Substitui a tag <util-panel> pelo template
            templateUrl: 'dng/dng-tooltip/dng-tooltip.html',
            scope: {
                dngTitle: '@dngTitle', //= two binding, @ one binding
                dngDirection: '@dngDirection',
                dngClass: '@dngClass'
            },
            compile: function(element, attrs) {
                attrs.dngDirection = (attrs.dngDirection === undefined) ? 'right' : attrs.dngDirection;
            }
        };
    }

})();