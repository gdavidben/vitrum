(function () {
	'use strict';
	angular
	.module('dng')
	.service('dngToastService', DngToastService);

	DngToastService.$inject = ['ngToast'];
	function DngToastService(ngToast) {
		var vm = this;
		

		vm.success = function(title, content) {
			toast('success', title, content);
		};

		vm.warning = function(title, content) {
			toast('warning', title, content);
		};

		vm.danger = function(title, content) {
			toast('danger', title, content);
		};

		function toast(clazz, title, content) {
			ngToast.create({
				content:'<b>' + title + '</b> ' + content,
				className: clazz,
				dismissOnTimeout: true,
				dismissButton: true,
				dismissOnClick: false,
				timeout: 10000
			});
		};
	}
})();