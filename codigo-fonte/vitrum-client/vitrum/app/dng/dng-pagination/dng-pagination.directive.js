(function () {
    'use strict';
    angular
            .module('dng')
            .directive('dngPagination', DngPaginationDirective);

    DngPaginationDirective.$inject = [];
    function DngPaginationDirective() {
        return {
            restrict: 'E',
            transclude: true,
            replace: true, //Substitui a tag <util-modal> pelo template
            templateUrl: 'dng/dng-pagination/dng-pagination.html',
            scope: {
                dngTotalPages: '=dngTotalPages', //= two binding, @ one binding
                dngCurrentPage: '=dngCurrentPage',
                dngMethod: '=dngMethod'
            },
            controller: function($scope) {
                $scope.pages = [];

                $scope.loadPage = loadPage;

                function loadPage(page) {
                    $scope.current = page;
                    $scope.dngMethod(page);
                }

                function load() {
                    for(var i = 0; i < $scope.dngTotalPages; i++) {
                        $scope.pages[i] = i;
                    }
                }

               load();
            }

        };
    }

})();