(function () {
    'use strict';
    angular
	.module('dng')
	.directive('dngFileModel', DngFileModelDirective);

    DngFileModelDirective.$inject = ['$parse'];
    function DngFileModelDirective($parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var model = $parse(attrs.dngFileModel);
				var modelSetter = model.assign;
 
				element.bind('change', function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files);
					})
				});
			}
        };
	}
})();