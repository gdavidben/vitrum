(function () {
    'use strict';
    angular
            .module('dng')
            .directive('dngEnter', DngEnterDirective);

    DngEnterDirective.$inject = [];
    function DngEnterDirective() {
		return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
					if(event.which === 9 || event.which === 13) {
						scope.$apply(function () {
							scope.$eval(attrs.dngEnter);
						});
						event.preventDefault();
	            	}
	        	});
            }
        };
    }
})();