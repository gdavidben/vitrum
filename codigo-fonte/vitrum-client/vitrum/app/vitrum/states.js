(function () {
    'use strict';
    
    var viewAuthentication = {
        templateUrl: 'vitrum/authentication/authentication.html',
        controller: 'authenticationController as authenticationController'
    };

    var viewHome = {
        templateUrl: 'vitrum/home/home.html',
        controller: 'homeController as homeController',
    };
    
    var viewChangePassword = {
        templateUrl: 'vitrum/changePassword/changePassword.html',
        controller: 'changePasswordController as changePasswordController'
    };
    
    var viewRestorePassword = {
        templateUrl: 'vitrum/restorePassword/restorePassword.html',
        controller: 'restorePasswordController as restorePasswordController'
    };

    var viewUser = {
        templateUrl: 'vitrum/user/user.html',
        controller: 'userController as userController'
    };
    
    var viewUsers = {
        templateUrl: 'vitrum/user/users.html',
        controller: 'usersController as usersController'
    };
    
    var viewCompany = {
        templateUrl: 'vitrum/company/company.html',
        controller: 'companyController as companyController'
    };
    
    var viewCompanies = {
        templateUrl: 'vitrum/company/companies.html',
        controller: 'companiesController as companiesController'
    };
    
    var viewFile = {
        templateUrl: 'vitrum/file/file.html',
        controller: 'fileController as fileController'
    };
    
    var viewFiles = {
        templateUrl: 'vitrum/file/files.html',
        controller: 'filesController as filesController'
    };
    
    var viewCampaign = {
        templateUrl: 'vitrum/campaign/campaign.html',
        controller: 'campaignController as campaignController'
    };
    
    var viewCampaigns = {
        templateUrl: 'vitrum/campaign/campaigns.html',
        controller: 'campaignsController as campaignsController'
    };
    
    var viewProgramming = {
        templateUrl: 'vitrum/programming/programming.html',
        controller: 'programmingController as programmingController'
    };
    
    var viewProgrammings = {
        templateUrl: 'vitrum/programming/programmings.html',
        controller: 'programmingsController as programmingsController'
    };
    
    var viewDevice = {
        templateUrl: 'vitrum/device/device.html',
        controller: 'deviceController as deviceController'
    };
    
    var viewDevices = {
        templateUrl: 'vitrum/device/devices.html',
        controller: 'devicesController as devicesController'
    };
    
    var stateAuthentication = {
        name: 'authentication',
        config: {
            url: '/authentication',
            views: {
                'main': viewAuthentication
            }
        }
    };

    var stateHome = {
        name: 'home',
        config: {
            url: '/',
            views: {
                'main': viewHome
            }
        }
    };
    
    var stateChangePassword = {
        name: 'changePassword',
        config: {
            url: '/changePassword',
            views: {
                'main': viewChangePassword
            }
        }
    };
    
    var stateRestorePassword = {
        name: 'restorePassword',
        config: {
            url: '/restorePassword',
            views: {
                'main': viewRestorePassword
            }
        }
    };

    var stateUser = {
        name: 'user',
        config: {
            url: '/users/:id',
            views: {
                'main': viewUser
            }
        }
    };
    
    var stateUsers = {
        name: 'users',
        config: {
            url: '/users',
            views: {
                'main': viewUsers
            }
        }
    };
    
    var stateCompany = {
        name: 'company',
        config: {
            url: '/companies/:id',
            views: {
                'main': viewCompany
            }
        }
    };
    
    var stateCompanies = {
        name: 'companies',
        config: {
            url: '/companies',
            views: {
                'main': viewCompanies
            }
        }
    };
    
    var stateFile = {
        name: 'file',
        config: {
            url: '/files/:id',
            views: {
                'main': viewFile
            }
        }
    };
    
    var stateFiles = {
        name: 'files',
        config: {
            url: '/files',
            views: {
                'main': viewFiles
            }
        }
    };

    var stateCampaign = {
        name: 'campaign',
        config: {
            url: '/campaigns/:id',
            views: {
                'main': viewCampaign
            }
        }
    };
    
    var stateCampaigns = {
        name: 'campaigns',
        config: {
            url: '/campaigns',
            views: {
                'main': viewCampaigns
            }
        }
    };

    var stateProgramming = {
        name: 'programming',
        config: {
            url: '/programmings/:id',
            views: {
                'main': viewProgramming
            }
        }
    };
    
    var stateProgrammings = {
        name: 'programmings',
        config: {
            url: '/programmings',
            views: {
                'main': viewProgrammings
            }
        }
    };

    var stateDevice = {
        name: 'device',
        config: {
            url: '/devices/:id',
            views: {
                'main': viewDevice
            }
        }
    };

    var stateDevices = {
        name: 'devices',
        config: {
            url: '/devices',
            views: {
                'main': viewDevices
            }
        }
    };

    var states = [
        stateAuthentication,
        stateHome,
        stateChangePassword,
        stateRestorePassword,
        stateUser,
        stateUsers,
        stateCompany,
        stateCompanies,
        stateFile,
        stateFiles,
        stateCampaign,
        stateCampaigns,
        stateProgramming,
        stateProgrammings,
        stateDevice, 
        stateDevices
    ];

    angular
            .module('vitrum')
            .constant("STATES", states);
})();