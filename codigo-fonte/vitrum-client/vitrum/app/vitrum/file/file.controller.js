(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('fileController', FileController);

	FileController.$inject = ['fileService', 'authenticationService', '$rootScope', 'dngToastService', '$http'];
	function FileController(fileService, authenticationService, $rootScope, dngToastService, $http) {
		var vm = this;

		vm.post = post;
		vm.files = undefined;
		vm.bytesToSize = bytesToSize;

		function post() {
			if(vm.files != undefined) {
				var formData = new FormData();
               	for(var i = 0; i < vm.files.length; i++) {
               		formData.append('file', vm.files[i]);
               	}

				fileService.create(formData).then(onCreateSuccess, onError);
			}
		};

		function bytesToSize(bytes) {
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
			if (bytes == 0) return '0 Byte';
				var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
			return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}

		function onCreateSuccess(response) {
			dngToastService.success('Arquivo registrado!', '');
			$rootScope.go('files');
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Arquivo alterado!', '');
			$rootScope.go('files');
		}

 		function onError(response) {
			var message = 'Ops!';
			var cause = 'Tente novamente daqui alguns minutos.';
			
			if(response != null && response.data != null) {
				if(response.data.message != null) {
					message = response.data.message;
				}
				if(response.data.cause != null) {
					cause = response.data.cause;
				}
			}
			dngToastService.danger(message, cause);
		}

		function load() {
			var id = $rootScope.$stateParams.id;

			if(id != "") {
				fileService.find(id).then(onFindSuccess, onError);
			}
		}

		function onFindSuccess(response) {
			vm.files = response.data;
		}

		load();
	}
})();