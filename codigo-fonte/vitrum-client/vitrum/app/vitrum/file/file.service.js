(function () {
    'use strict';
    angular
    .module('vitrum')
    .factory('fileService', FileService);

    FileService.$inject = ['httpService', 'CONSTANT'];
    function FileService(httpService, CONSTANT) {
        var URL = CONSTANT.API_URL + '/files';

        function create(object) {
            return httpService.postFile(URL + '/upload', object);
        }

        function update(object) {
            return httpService.put(URL + '/' + object.id, object);
        }

        function remove(fileName) {
            return httpService.remove(URL + '/' + fileName);
        }

        function findAllByKeyword(page, size, keyword, fileType) {
            if(page == undefined) {
                page = 0;
            }
            return httpService.get(URL + '?page=' + page + '&size=' + size + '&keyword=' + keyword + '&fileType=' + fileType);
        }

        function find(id) {
            return httpService.get(URL + '/' + id);
        }

        return {
            create : create,
            update : update,
            remove : remove,
            findAllByKeyword : findAllByKeyword,
            find : find
        };
    }
})();