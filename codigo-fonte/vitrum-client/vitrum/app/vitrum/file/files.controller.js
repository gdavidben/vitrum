(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('filesController', FilesController);

	FilesController.$inject = ['fileService', '$rootScope', 'dngToastService', '$mdDialog'];
	function FilesController(fileService, $rootScope, dngToastService, $mdDialog) {		
		var vm = this;

		vm.keyword = "";
		vm.fileType = "IMAGES";
		vm.findAllByKeyword = findAllByKeyword;
		vm.confirmRemove = confirmRemove;
		vm.confirmUpdate = confirmUpdate;

		function load() {
			findAllByKeyword(0);
		}

		function findAllByKeyword(page) {
			fileService.findAllByKeyword(page, 100, vm.keyword, vm.fileType).then(onFindAllByKeywordSuccess, onError);
		}

		function onFindAllByKeywordSuccess(response) {
			vm.data = response.data;
		}

		function confirmUpdate(file) {
			var action = (file.enabled) ? 'desativar' : 'ativar';
			var confirm = $mdDialog.confirm()
      			.title('Deseja ' + action + ' o arquivo ' + file.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				update(file);
			}, undefined);
		}

		function update(file) {
			file.enabled = (file.enabled) ? false : true;
			fileService.update(file).then(onUpdateSuccess, onError);
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Arquivo alterado!', '');
			findAllByKeyword(0);
		}

		function confirmRemove(file) {
			var confirm = $mdDialog.confirm()
      			.title('Deseja remover o arquivo ' + file.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				remove(file.name);
			}, undefined);
		}

		function remove(fileName) {
			fileService.remove(fileName).then(onRemoveSuccess, onError);
		}

		function onRemoveSuccess(response) {
			dngToastService.success('Arquivo removido!', '');
			findAllByKeyword(0);
		}

		function onError(response) {
			dngToastService.danger('Algo inesperado aconteceu!', 'Tente novamente daqui alguns minutos.');
			vm.data = undefined;
		}

		load();
	}
})();