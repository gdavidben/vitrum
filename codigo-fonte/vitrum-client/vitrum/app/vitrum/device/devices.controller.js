(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('devicesController', DevicesController);

	DevicesController.$inject = ['deviceService', '$rootScope', 'dngToastService', '$mdDialog'];
	function DevicesController(deviceService, $rootScope, dngToastService, $mdDialog) {		
		var vm = this;

		vm.keyword = "";
		vm.findAllByKeyword = findAllByKeyword;
		vm.confirmRemove = confirmRemove;
		vm.confirmUpdate = confirmUpdate;

		function load() {
			findAllByKeyword(0);
		}

		function findAllByKeyword(page) {
			deviceService.findAllByKeyword(page, 100, vm.keyword).then(onFindAllByKeywordSuccess, onError);
		}

		function onFindAllByKeywordSuccess(response) {
			vm.data = response.data;
		}

		function confirmUpdate(device) {
			var action = (device.enabled) ? 'desativar' : 'ativar';
			var confirm = $mdDialog.confirm()
      			.title('Deseja ' + action + ' o dispositivo ' + device.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				update(device);
			}, undefined);
		}

		function update(device) {
			device.enabled = (device.enabled) ? false : true;
			deviceService.update(device).then(onUpdateSuccess, onError);
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Dispositivo alterado!', '');
			findAllByKeyword(0);
		}

		function confirmRemove(device) {
			var confirm = $mdDialog.confirm()
      			.title('Deseja remover o dispositivo ' + device.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				remove(device);
			}, undefined);
		}

		function remove(device) {
			deviceService.remove(device.id).then(onRemoveSuccess, onError);
		}

		function onRemoveSuccess(response) {
			dngToastService.success('Dispositivo removido!', '');
			findAllByKeyword(0);
		}

		function onError(response) {
			dngToastService.danger('Algo inesperado aconteceu!', 'Tente novamente daqui alguns minutos.');
			vm.data = undefined;
		}

		load();
	}
})();