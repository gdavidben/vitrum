(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('deviceController', DeviceController);

	DeviceController.$inject = ['deviceService', 'authenticationService', '$rootScope', 'dngToastService'];
	function DeviceController(deviceService, authenticationService, $rootScope, dngToastService) {
		var vm = this;

		vm.post = post;

		function post() {
			if(vm.device.id != undefined)
				deviceService.update(vm.device).then(onUpdateSuccess, onError);
			else
				deviceService.create(vm.device).then(onCreateSuccess, onError);
		};

		function onCreateSuccess(response) {
			dngToastService.success('Dispositivo registrado!', '');
			$rootScope.go('devices');
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Dispositivo alterado!', '');
			$rootScope.go('devices');
		}

 		function onError(response) {
			var message = 'Ops!';
			var cause = 'Tente novamente daqui alguns minutos.';
			
			if(response != null && response.data != null) {
				if(response.data.message != null) {
					message = response.data.message;
				}
				if(response.data.cause != null) {
					cause = response.data.cause;
				}
			}
			dngToastService.danger(message, cause);
		}

		function load() {
			var id = $rootScope.$stateParams.id;

			if(id != "") {
				deviceService.find(id).then(onFindSuccess, onError);
			}
		}

		function onFindSuccess(response) {
			vm.device = response.data;
		}

		load();
	}
})();