(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('programmingsController', ProgrammingsController);

	ProgrammingsController.$inject = ['programmingService', '$rootScope', 'dngToastService', '$mdDialog'];
	function ProgrammingsController(programmingService, $rootScope, dngToastService, $mdDialog) {		
		var vm = this;

		vm.keyword = "";
		vm.findAllByKeyword = findAllByKeyword;
		vm.confirmRemove = confirmRemove;
		vm.confirmUpdate = confirmUpdate;

		function load() {
			findAllByKeyword(0);
		}

		function findAllByKeyword(page) {
			programmingService.findAllByKeyword(page, 100, vm.keyword).then(onFindAllByKeywordSuccess, onError);
		}

		function onFindAllByKeywordSuccess(response) {
			vm.data = response.data;
		}

		function confirmUpdate(programming) {
			var action = (programming.enabled) ? 'desativar' : 'ativar';
			var confirm = $mdDialog.confirm()
      			.title('Deseja ' + action + ' a programação ' + programming.description + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				update(programming);
			}, undefined);
		}

		function update(programming) {
			programming.enabled = (programming.enabled) ? false : true;
			programmingService.update(programming).then(onUpdateSuccess, onError);
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Programação alterado!', '');
			findAllByKeyword(0);
		}

		function confirmRemove(programming) {
			var confirm = $mdDialog.confirm()
      			.title('Deseja remover o programação ' + programming.description + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				remove(programming);
			}, undefined);
		}

		function remove(programming) {
			programmingService.remove(programming.id).then(onRemoveSuccess, onError);
		}

		function onRemoveSuccess(response) {
			dngToastService.success('Programação removida!', '');
			findAllByKeyword(0);
		}

		function onError(response) {
			dngToastService.danger('Algo inesperado aconteceu!', 'Tente novamente daqui alguns minutos.');
			vm.data = undefined;
		}

		load();
	}
})();