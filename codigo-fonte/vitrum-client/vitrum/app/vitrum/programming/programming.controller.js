(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('programmingController', ProgrammingController);

	ProgrammingController.$inject = ['programmingService', 'authenticationService', '$rootScope', 'dngToastService'];
	function ProgrammingController(programmingService, authenticationService, $rootScope, dngToastService) {
		var vm = this;

		vm.post = post;

		function post() {
			if(vm.programming.id != undefined)
				programmingService.update(vm.programming).then(onUpdateSuccess, onError);
			else
				programmingService.create(vm.programming).then(onCreateSuccess, onError);
		};

		function onCreateSuccess(response) {
			dngToastService.success('Programação registrada!', '');
			$rootScope.go('programmings');
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Programação alterada!', '');
			$rootScope.go('programmings');
		}

 		function onError(response) {
			var message = 'Ops!';
			var cause = 'Tente novamente daqui alguns minutos.';
			
			if(response != null && response.data != null) {
				if(response.data.message != null) {
					message = response.data.message;
				}
				if(response.data.cause != null) {
					cause = response.data.cause;
				}
			}
			dngToastService.danger(message, cause);
		}

		function load() {
			var id = $rootScope.$stateParams.id;
			
			if(id != "") {
				programmingService.find(id).then(onFindSuccess, onError);
			}
		}

		function onFindSuccess(response) {
			vm.programming = response.data;
		}

		load();
	}
})();