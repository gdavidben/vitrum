(function () {
    'use strict';
    angular
    .module('vitrum')
    .factory('campaignService', CampaignService);

    CampaignService.$inject = ['httpService', 'CONSTANT'];
    function CampaignService(httpService, CONSTANT) {
        var URL = CONSTANT.API_URL + '/campaigns';
        
        function create(object) {
            return httpService.post(URL, object);
        }

        function update(object) {
            return httpService.put(URL + '/' + object.id, object);
        }

        function remove(id) {
            return httpService.remove(URL + '/' + id, undefined);
        }

        function findAllByKeyword(page, size, keyword) {
            if(page == undefined) {
                page = 0;
            }
            return httpService.get(URL + '?page=' + page + '&size=' + size + '&keyword=' + keyword);
        }

        function find(id) {
            return httpService.get(URL + '/' + id);
        }

        return {
            create : create,
            update : update,
            remove : remove,
            findAllByKeyword : findAllByKeyword,
            find : find
        };
    }
})();