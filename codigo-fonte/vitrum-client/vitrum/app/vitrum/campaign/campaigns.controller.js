(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('campaignsController', CampaignsController);

	CampaignsController.$inject = ['campaignService', '$rootScope', 'dngToastService', '$mdDialog'];
	function CampaignsController(campaignService, $rootScope, dngToastService, $mdDialog) {		
		var vm = this;

		vm.keyword = "";
		vm.findAllByKeyword = findAllByKeyword;
		vm.confirmRemove = confirmRemove;
		vm.confirmUpdate = confirmUpdate;

		function load() {
			findAllByKeyword(0);
		}

		function findAllByKeyword(page) {
			campaignService.findAllByKeyword(page, 100, vm.keyword).then(onFindAllByKeywordSuccess, onError);
		}

		function onFindAllByKeywordSuccess(response) {
			vm.data = response.data;
		}

		function confirmUpdate(compaign) {
			var action = (compaign.enabled) ? 'desativar' : 'ativar';
			var confirm = $mdDialog.confirm()
      			.title('Deseja ' + action + ' a campanha ' + compaign.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				update(compaign);
			}, undefined);
		}

		function update(compaign) {
			compaign.enabled = (compaign.enabled) ? false : true;
			campaignService.update(compaign).then(onUpdateSuccess, onError);
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Campanha alterada!', '');
			findAllByKeyword(0);
		}

		function confirmRemove(compaign) {
			var confirm = $mdDialog.confirm()
      			.title('Deseja remover a campanha ' + compaign.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				remove(compaign);
			}, undefined);
		}

		function remove(compaign) {
			campaignService.remove(compaign.id).then(onRemoveSuccess, onError);
		}

		function onRemoveSuccess(response) {
			dngToastService.success('Campanha removida!', '');
			findAllByKeyword(0);
		}

		function onError(response) {
			dngToastService.danger('Algo inesperado aconteceu!', 'Tente novamente daqui alguns minutos.');
			vm.data = undefined;
		}

		load();
	}
})();