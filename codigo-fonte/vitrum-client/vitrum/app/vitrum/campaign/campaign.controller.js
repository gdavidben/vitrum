(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('campaignController', CampaignController);

	CampaignController.$inject = ['campaignService', 'authenticationService', '$rootScope', 'dngToastService'];
	function CampaignController(campaignService, authenticationService, $rootScope, dngToastService) {
		var vm = this;

		vm.post = post;

		function post() {
			if(vm.campaign.id != undefined)
				campaignService.update(vm.campaign).then(onUpdateSuccess, onError);
			else
				campaignService.create(vm.campaign).then(onCreateSuccess, onError);
		};

		function onCreateSuccess(response) {
			dngToastService.success('Campanha registrado!', 'Sua senha foi enviada por e-mail.');
			$rootScope.go('campaigns');
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Campanha alterado!', '');
			$rootScope.go('campaigns');
		}

 		function onError(response) {
			var message = 'Ops!';
			var cause = 'Tente novamente daqui alguns minutos.';
			
			if(response != null && response.data != null) {
				if(response.data.message != null) {
					message = response.data.message;
				}
				if(response.data.cause != null) {
					cause = response.data.cause;
				}
			}
			dngToastService.danger(message, cause);
		}

		function load() {
			var id = $rootScope.$stateParams.id;
			
			if(id != "") {
				campaignService.find(id).then(onFindSuccess, onError);
			}
		}

		function onFindSuccess(response) {
			vm.campaign = response.data;
		}

		load();
	}
})();