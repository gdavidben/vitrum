(function () {
    'use strict';
    angular
            .module('vitrum')
            .run(Run);

    Run.$inject = ['$stateParams', '$rootScope', '$state', 'authenticationService'];
    
    function Run($stateParams, $rootScope, $state, authenticationService) {
        $rootScope.$stateParams = $stateParams;
        $rootScope.go = function (stateName, params) {
            $state.go(stateName, params);
        };

        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
            var authToken = authenticationService.getAuthToken();
            var currentUser = authenticationService.getCurrentUser();
            
            if((authToken == undefined || authToken == "null") && toState.name != "authentication" && toState.name != "restorePassword") {
                $state.transitionTo("authentication");
                event.preventDefault();
            }else if((authToken != undefined && authToken != "null") && toState.name == "authentication") {
                $state.transitionTo("home");
                event.preventDefault();
            }else if(authToken != undefined && currentUser != undefined && currentUser.passwordTemporary == true) {
                $state.transitionTo("changePassword");
                event.preventDefault();
            }

        });
    }
})();