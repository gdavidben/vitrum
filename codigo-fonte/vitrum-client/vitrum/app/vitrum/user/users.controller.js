(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('usersController', UsersController);

	UsersController.$inject = ['userService', '$rootScope', 'dngToastService', '$mdDialog'];
	function UsersController(userService, $rootScope, dngToastService, $mdDialog) {		
		var vm = this;

		vm.keyword = "";
		vm.findAllByKeyword = findAllByKeyword;
		vm.confirmRemove = confirmRemove;
		vm.confirmUpdate = confirmUpdate;

		function load() {
			findAllByKeyword(0);
		}

		function findAllByKeyword(page) {
			userService.findAllByKeyword(page, 100, vm.keyword).then(onFindAllByKeywordSuccess, onError);
		}

		function onFindAllByKeywordSuccess(response) {
			vm.data = response.data;
		}

		function confirmUpdate(user) {
			var action = (user.enabled) ? 'desativar' : 'ativar';
			var confirm = $mdDialog.confirm()
      			.title('Deseja ' + action + ' o usuário ' + user.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				update(user);
			}, undefined);
		}

		function update(user) {
			user.enabled = (user.enabled) ? false : true;
			userService.update(user).then(onUpdateSuccess, onError);
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Usuário alterado!', '');
			findAllByKeyword(0);
		}

		function confirmRemove(user) {
			var confirm = $mdDialog.confirm()
      			.title('Deseja remover o usuário ' + user.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				remove(user);
			}, undefined);
		}

		function remove(user) {
			userService.remove(user.id).then(onRemoveSuccess, onError);
		}

		function onRemoveSuccess(response) {
			dngToastService.success('Usuário removido!', '');
			findAllByKeyword(0);
		}

		function onError(response) {
			dngToastService.danger('Algo inesperado aconteceu!', 'Tente novamente daqui alguns minutos.');
			vm.data = undefined;
		}

		load();
	}
})();