(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('userController', UserController);

	UserController.$inject = ['userService', 'authenticationService', '$rootScope', 'dngToastService'];
	function UserController(userService, authenticationService, $rootScope, dngToastService) {
		var vm = this;

		vm.post = post;

		function post() {
			if(vm.user.id != undefined)
				userService.update(vm.user).then(onUpdateSuccess, onError);
			else
				userService.create(vm.user).then(onCreateSuccess, onError);
		};

		function onCreateSuccess(response) {
			dngToastService.success('Usuário registrado!', 'Sua senha foi enviada por e-mail.');
			$rootScope.go('users');
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Usuário alterado!', '');
			$rootScope.go('users');
		}

 		function onError(response) {
			var message = 'Ops!';
			var cause = 'Tente novamente daqui alguns minutos.';
			
			if(response != null && response.data != null) {
				if(response.data.message != null) {
					message = response.data.message;
				}
				if(response.data.cause != null) {
					cause = response.data.cause;
				}
			}
			dngToastService.danger(message, cause);
		}

		function load() {
			var id = $rootScope.$stateParams.id;
			var currentUser = authenticationService.getCurrentUser();

			vm.companies = currentUser.companies;
			vm.authorities = currentUser.authorities;

			if(id != "") {
				userService.find(id).then(onFindSuccess, onError);
			}
		}

		function onFindSuccess(response) {
			vm.user = response.data;
		}

		load();
	}
})();