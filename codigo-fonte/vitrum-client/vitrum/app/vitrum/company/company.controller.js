(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('companyController', CompanyController);

	CompanyController.$inject = ['companyService', 'authenticationService', '$rootScope', 'dngToastService'];
	function CompanyController(companyService, authenticationService, $rootScope, dngToastService) {
		var vm = this;

		vm.post = post;

		function post() {
			if(vm.company.id != undefined)
				companyService.update(vm.company).then(onUpdateSuccess, onError);
			else
				companyService.create(vm.company).then(onCreateSuccess, onError);
		};

		function onCreateSuccess(response) {
			dngToastService.success('Empresa registrada!', '');
			$rootScope.go('companies');
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Empresa alterada!', '');
			$rootScope.go('companies');
		}

 		function onError(response) {
			var message = 'Ops!';
			var cause = 'Tente novamente daqui alguns minutos.';
			
			if(response != null && response.data != null) {
				if(response.data.message != null) {
					message = response.data.message;
				}
				if(response.data.cause != null) {
					cause = response.data.cause;
				}
			}
			dngToastService.danger(message, cause);
		}

		function load() {
			var id = $rootScope.$stateParams.id;
			var currentUser = authenticationService.getCurrentUser();

			vm.companies = currentUser.companies;
			vm.authorities = currentUser.authorities;

			if(id != "") {
				companyService.find(id).then(onFindSuccess, onError);
			}
		}

		function onFindSuccess(response) {
			vm.company = response.data;
		}

		load();
	}
})();