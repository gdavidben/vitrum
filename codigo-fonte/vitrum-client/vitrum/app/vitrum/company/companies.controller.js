(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('companiesController', CompaniesController);

	CompaniesController.$inject = ['companyService', '$rootScope', 'dngToastService', '$mdDialog'];
	function CompaniesController(companyService, $rootScope, dngToastService, $mdDialog) {		
		var vm = this;

		vm.keyword = "";
		vm.findAllByKeyword = findAllByKeyword;
		vm.confirmRemove = confirmRemove;
		vm.confirmUpdate = confirmUpdate;

		function load() {
			findAllByKeyword(0);
		}

		function findAllByKeyword(page) {
			companyService.findAllByKeyword(page, 100, vm.keyword).then(onFindAllByKeywordSuccess, onError);
		}

		function onFindAllByKeywordSuccess(response) {
			vm.data = response.data;
		}

		function confirmUpdate(companie) {
			var action = (companie.enabled) ? 'desativar' : 'ativar';
			var confirm = $mdDialog.confirm()
      			.title('Deseja ' + action + ' a empresa ' + companie.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				update(companie);
			}, undefined);
		}

		function update(companie) {
			companie.enabled = (companie.enabled) ? false : true;
			companyService.update(companie).then(onUpdateSuccess, onError);
		}

		function onUpdateSuccess(response) {
			dngToastService.success('Empresa alterada!', '');
			findAllByKeyword(0);
		}

		function confirmRemove(companie) {
			var confirm = $mdDialog.confirm()
      			.title('Deseja remover a empresa ' + companie.name + '?')
      			.textContent('')
      			.ok('Confirmar')
      			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				remove(companie);
			}, undefined);
		}

		function remove(companie) {
			companyService.remove(companie.id).then(onRemoveSuccess, onError);
		}

		function onRemoveSuccess(response) {
			dngToastService.success('Empresa removida!', '');
			findAllByKeyword(0);
		}

		function onError(response) {
			dngToastService.danger('Algo inesperado aconteceu!', 'Tente novamente daqui alguns minutos.');
			vm.data = undefined;
		}

		load();
	}
})();