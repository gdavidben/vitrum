(function () {
    'use strict';
    angular
            .module('vitrum')
            .controller('changePasswordController', ChangePasswordController);

    ChangePasswordController.$inject = ['authenticationService', 'dngToastService'];
    function ChangePasswordController(authenticationService, dngToastService) {
        var vm = this;

        vm.changePassword = changePassword;

        function changePassword() {
        	if(vm.user.newPassword == vm.user.repeatNewPassword) {
        		authenticationService.changePassword(vm.user).then(onChangePasswordSuccess, onChangePasswordError);
        	} else {
        		dngToastService.danger('Senhas divergentes!', 'Repita a senha escolhida.');
        	}
        }

		function onChangePasswordSuccess(response) {
			dngToastService.success('Senha redefinida!', '');
		}

		function onChangePasswordError(response) {
			dngToastService.danger('Erro ao redefinir senha!', 'Tente novamente.');
		}
    }
})();