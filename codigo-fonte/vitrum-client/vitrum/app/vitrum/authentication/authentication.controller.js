(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('authenticationController', AuthenticationController);

	AuthenticationController.$inject = ['authenticationService', '$state', 'dngToastService', 'vcRecaptchaService'];
	function AuthenticationController(authenticationService, $state, dngToastService, vcRecaptchaService) {
		var vm = this;
		vm.login = login;

		function login() {
			authenticationService.login(vm.user).then(onLoginSuccess, onLoginError);
		}

		function onLoginSuccess(response) {
			$state.go('home');
		}

		function onLoginError(response) {
			if(response.status == -1) {
				dngToastService.danger('Serviço indiponível!', 'Tente novamente daqui alguns minutos.');
			}else {
				dngToastService.danger('Login inválido!', 'Tente novamente.');	
			}
		}

		function load() {
			vm.user = {
				username : 'suportevitrum@gmail.com',
				password : '123456'
			};
		}

		load();

	}
})();