(function () {
	'use strict';
	angular
	.module('vitrum')
	.factory('authenticationService', AuthenticationService);

	AuthenticationService.$inject = ['httpService', 'CONSTANT', '$cookies'];
	function AuthenticationService(httpService, CONSTANT, $cookies) {

		function login(user) {
			var promise = httpService.post(CONSTANT.API_URL + '/login', user);
			promise.then(onLoginSuccess, onLoginError);
			return promise;
		}

		function onLoginSuccess(response) {		
 			if(response.data != undefined) {
				var currentUser = btoa(JSON.stringify(response.data));
 				var currentCompany = btoa(JSON.stringify(response.data.companies[0]));
 				httpService.loadCookies(undefined, currentUser, currentCompany);
 			}else {
 				logout();
 			}
		}

		function onLoginError(response) {
			logout();
		}

		function logout() {
			httpService.clearCookies();
		}

		function getAuthToken() {
			var authToken = $cookies.get('Auth-Token');

			if(authToken != undefined && authToken != "null") {
				return authToken;
			} else {
				return undefined;
			}
		}

		function getCurrentUser() {
			var currentUser = $cookies.get('Current-User');
			
			if(currentUser != undefined && currentUser != "null") {
				return JSON.parse(atob(currentUser.replace('\"', '').replace('"', '')));
			} else {
				return undefined;
			}
		}

		function getCurrentCompany() {
			var currentCompany = $cookies.get('Current-Company');
			
			if(currentCompany != undefined && currentCompany != "null") {
				return JSON.parse(atob(currentCompany.replace('\"', '').replace('"', '')));
			} else {
				return undefined;
			}
		}

		function setCurrentCompany(currentCompany) {
			if(currentCompany != undefined) {
				currentCompany = btoa(JSON.stringify(currentCompany));	
			}
			httpService.loadCookies(undefined, undefined, currentCompany);
		}

		function registerCallback(callback) {
			httpService.registerCallback(callback);
		}

		function restorePassword(user, onRestorePasswordSuccess, onRestorePasswordError) {
			return httpService.put(CONSTANT.API_URL + '/users/restorePassword', user).then(onRestorePasswordSuccess, onRestorePasswordError);
		}

		function changePassword(user, onChancePasswordSuccess, onChangePasswordError) {
			return httpService.put(CONSTANT.API_URL + '/users/changePassword', user).then(onChancePasswordSuccess, onChangePasswordError);
		}

		return {
			login : login,
			logout : logout,
			getAuthToken : getAuthToken,
			getCurrentUser : getCurrentUser,
			getCurrentCompany : getCurrentCompany,
			setCurrentCompany : setCurrentCompany,
			registerCallback : registerCallback,
			restorePassword : restorePassword,
			changePassword : changePassword
		};

	}
})();