(function () {
	'use strict';
	angular
	.module('vitrum')
	.service('httpService', HttpService);

	HttpService.$inject = ['$http', '$q', '$timeout', '$cookies', '$rootScope', 'dngToastService'];
	function HttpService($http, $q, $timeout, $cookies, $rootScope, dngToastService) {
		var callbacks = [];
		var vm = this;

		vm.get = get;
		vm.post = post;
		vm.postFile = postFile;
		vm.put = put;
		vm.remove = remove;
		vm.registerCallback = registerCallback;
		vm.clearCookies = clearCookies;
		vm.loadCookies = loadCookies;

		function callHttp(method, url, headers, params, data, uploadFileMode) {
			var task = $q.defer();

			var config = {
				method: method,
				url: url,
				params: params,
				data: data,
				headers: headers,
				withCredentials: true
			};

			if(uploadFileMode) {
				config.transformRequest = angular.identity;			
			}

			NProgress.start();
			NProgress.inc();
			
			$timeout(function () {
				$http(config)
				.success(function (data, status, headers, config) {
					var authToken = headers('Auth-Token');
					loadCookies(authToken);
					NProgress.done();
					task.resolve({data: data, status: status, headers: headers, config: config});
				})
				.error(function (data, status, headers, config) {
					NProgress.done();

					if(status == 401 || status == -1 || status == 500) {
						clearCookies();
						dngToastService.warning('Sessão encerrada!', 'Realize a autenticação novamente.');
						$rootScope.go('authentication');
					} else if(status == 403) {
						dngToastService.warning('Acesso negado!', 'Você não possui permissão para acesar essa funcionalidade.');
						$rootScope.go('home');
					} else {
						task.reject({data: data, status: status, headers: headers, config: config});	
					}					
				});
			}, 0);

			return task.promise;
		}

		function get(url, params) {
			return callHttp('GET', url, loadHeaders(), params);
		};

		function post(url, data) {
			return callHttp('POST', url, loadHeaders(), undefined, data);
		};

		function postFile(url, data) {
			var headers = loadHeaders();
			delete headers["Content-Type"];
			headers["Content-Type"] = undefined;
			return callHttp('POST', url, headers, undefined, data, true);
		};

		function put(url, data) {
			return callHttp('PUT', url, loadHeaders(), undefined, data);
		};

		function remove(url, params) {
			return callHttp('DELETE', url, loadHeaders(), params);
		};

		function loadHeaders() {
			var headers = {
				"Accept" : "application/json",
				"Content-Type" : "application/json; charset=UTF-8",
				"Cache-Control" : "no-cache", 
				"X-XSS-Protection" : "1; mode=block", 
				"X-Frame-Options" : "DENY",
				"X-Campaign-Options": "DENY",
				"Access-Control-Allow-Methods" : "GET, POST, PUT, DELETE, OPTIONS",
				"Access-Control-Allow-Credentials" : "true",
				"Access-Control-Allow-Origin" : "http://localhost:8080",
				"Access-Control-Expose-Headers" : "Auth-Token",
				"Access-Control-Allow-Headers" : "Accept, Content-Type, Cache-Control, X-XSS-Protection, X-Frame-Options, X-Campaign-Options, Access-Control-Allow-Methods, Access-Control-Allow-Credentials, Access-Control-Allow-Origin, Access-Control-Expose-Headers, Access-Control-Allow-Headers"
			};
			return headers;
		}

		function loadCookies(authToken, currentUser, currentCompany) {
			if(authToken != undefined) {
				$cookies.remove('Auth-Token');
 				$cookies.put('Auth-Token', authToken);
			}
			if(currentUser != undefined) {
				$cookies.remove('Current-User');
 				$cookies.put('Current-User', currentUser);
			}
			if(currentCompany != undefined) {
				$cookies.remove('Current-Company');
 				$cookies.put('Current-Company', currentCompany);
			}
 			notifyCallback();
		}

		function clearCookies() {
			$cookies.remove('Auth-Token');
			$cookies.remove('Current-User');
			$cookies.remove('Current-Company');
 			notifyCallback();
		}

		function registerCallback(callback) {
			callbacks.push(callback);
		}

		function notifyCallback() {
			angular.forEach(callbacks, function(callback) {
				callback();
			});
		};

	}
})();