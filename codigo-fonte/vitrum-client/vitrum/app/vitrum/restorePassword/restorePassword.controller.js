(function () {
	'use strict';
	angular
	.module('vitrum')
	.controller('restorePasswordController', RestorePasswordController);

	RestorePasswordController.$inject = ['authService', '$state', 'dngToastService', 'vcRecaptchaService'];
	function RestorePasswordController(authService, $state, dngToastService, vcRecaptchaService) {
		var vm = this;
		vm.restorePassword = restorePassword;

		function restorePassword() {
			authService.restorePassword(vm.user).then(onRestorePasswordSuccess, onRestorePasswordError);
		}

		function onRestorePasswordSuccess(response) {
			$state.go('login');
			dngToastService.success('Senha redefinida!', 'Siga as instruções que foram enviadas por e-mail.');
		}

		function onRestorePasswordError(response) {
			dngToastService.danger('Erro ao redefinir senha!', 'Tente novamente.');
		}

	}
})();