(function () {
    'use strict';
    angular
    .module('vitrum')
    .config(Config);

    Config.$inject = ['$stateProvider', '$urlRouterProvider', 'STATES'];
    function Config($stateProvider, $urlRouterProvider, STATES) {
        for (var i = 0; i < STATES.length; i++) {
            var state = STATES[i];
            $stateProvider.state(state.name, state.config);
        }
        $urlRouterProvider.otherwise('/');
    }
})();