(function () {
    'use strict';
    angular
    .module('vitrum')
    .controller('headerController', HeaderController);

    HeaderController.$inject = ['authenticationService', '$state', 'dngToastService', '$scope'];
    function HeaderController(authenticationService, $state, dngToastService, $scope) {
        var vm = $scope;
        vm.logout = logout;
        vm.changeCompany = changeCompany;

        function logout() {
            authenticationService.logout();
            $state.go('authentication');
            dngToastService.success('Acesso encerrado!', 'Realize o login novamente.');
        }

        function changeCompany(currentCompany) {
            authenticationService.setCurrentCompany(currentCompany);
            $state.reload();
        }

        function loadCallback() {
            vm.authToken = authenticationService.getAuthToken();
            vm.currentUser = authenticationService.getCurrentUser();
            vm.currentCompany = authenticationService.getCurrentCompany();
            validateAuthorities();
        }

        function load() {
            loadCallback();
            authenticationService.registerCallback(loadCallback);
        }

        function validateAuthorities() {
            if(vm.currentUser != undefined) {
                var authorities = vm.currentUser.authorities;
                if(authorities != undefined) {
                    for(var i = 0; i < authorities.length; i++) {
                        if(authorities[i].authority === 'ROLE_USER') {
                            vm.roleUser = true;
                        } else  if(authorities[i].authority === 'ROLE_COMPANY') {
                            vm.roleCompany = true;
                        } else  if(authorities[i].authority === 'ROLE_FILE') {
                            vm.roleFile = true;
                        } else  if(authorities[i].authority === 'ROLE_CAMPAIGN') {
                            vm.roleCampaign = true;
                        } else  if(authorities[i].authority === 'ROLE_PROGRAMMING') {
                            vm.roleProgramming = true;
                        } else  if(authorities[i].authority === 'ROLE_DEVICE') {
                            vm.roleDevice = true;
                        }
                    }
                }
            }
        }

        load();
    }

})();