npm install
bower install

# vitrum-client

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## Referências

Cross Site Request Forgery (XSRF) Protection https://docs.angularjs.org/api/ng/service/$http
http://www.codesandnotes.be/2015/07/12/angularjs-web-apps-for-spring-based-rest-services-security-the-server-side-part-1/