package com.vitrum.auth.jwt.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author DavidBen
 *
 */
public class CorsFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		if(req instanceof HttpServletRequest && res instanceof HttpServletResponse) {
			HttpServletResponse response = (HttpServletResponse) res;

			response.setHeader("Accept", "application/json");
			response.setHeader("Content-Type", "application/json; charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("X-XSS-Protection", "1; mode=block");
			response.setHeader("X-Frame-Options",  "DENY");
			response.setHeader("X-Campaign-Options", "DENY");
			response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
			response.setHeader("Access-Control-Allow-Credentials", "true");
			response.setHeader("Access-Control-Allow-Origin", "http://localhost:9000");
			response.setHeader("Access-Control-Expose-Headers", "Auth-Token");
			response.setHeader("Access-Control-Allow-Headers", "Accept, Content-Type, Cache-Control, X-XSS-Protection, X-Frame-Options, X-Campaign-Options, Access-Control-Allow-Methods, Access-Control-Allow-Credentials, Access-Control-Allow-Origin, Access-Control-Expose-Headers, Access-Control-Allow-Headers");
			
		}
		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {

	}

}
