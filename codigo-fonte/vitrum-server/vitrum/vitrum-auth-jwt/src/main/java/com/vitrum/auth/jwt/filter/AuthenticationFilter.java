package com.vitrum.auth.jwt.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitrum.entity.User;
import com.vitrum.service.AuthenticationService;

/**
 * @author DavidBen
 *
 */
public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	@Autowired
	private AuthenticationService authenticationService;
	
	public AuthenticationFilter(String urlMapping, AuthenticationManager authenticationManager) {
		super(new AntPathRequestMatcher(urlMapping, HttpMethod.POST.toString()));
		setAuthenticationManager(authenticationManager);
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
		final User user = new ObjectMapper().readValue(request.getInputStream(), User.class);
		final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
		Authentication auth = null;
		
		try {
			auth = getAuthenticationManager().authenticate(usernamePasswordAuthenticationToken);
		} catch (InternalAuthenticationServiceException e) {
			throw new com.vitrum.exception.AuthenticationException("Erro ao autenticar o usuario: " + user.getUsername());
		}
		return auth;
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
		authenticationService.addAuthHeaderResponse(response, authentication);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		ObjectMapper objectMapper = new ObjectMapper();
		User user = authenticationService.getCurrentUser();
		String jsonInString = objectMapper.writeValueAsString(user);
		response.getWriter().write(jsonInString);
	}
	
	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
		super.unsuccessfulAuthentication(request, response, failed);
	}
}
