package com.vitrum.auth.jwt.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.vitrum.service.AuthenticationService;

/**
 * @author DavidBen
 *
 */
public class ValidationTokenFilter extends GenericFilterBean {

	@Autowired
	private AuthenticationService authenticationService;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		Authentication authentication = authenticationService.getUsernamePasswordAuthenticationToken((HttpServletRequest) request, (HttpServletResponse) response);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		filterChain.doFilter(request, response);
		SecurityContextHolder.clearContext();
		authenticationService.validateAccessCompany(request);
	} 
}
