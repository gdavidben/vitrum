package com.vitrum.auth.jwt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.dao.ReflectionSaltSource;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.vitrum.auth.jwt.filter.AuthenticationFilter;
import com.vitrum.auth.jwt.filter.CorsFilter;
import com.vitrum.auth.jwt.filter.ValidationTokenFilter;
import com.vitrum.service.AuthenticationService;

/**
 * @author DavidBen
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationService authenticationService;

	public WebSecurityConfig() {
		super(true);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.exceptionHandling().and()
			.anonymous().and()
			//.servletApi().and()
			.authorizeRequests()
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
			.antMatchers(HttpMethod.PUT, "/users/restorePassword").permitAll()
			.antMatchers(HttpMethod.POST, "/login").permitAll()
			.antMatchers("/campaigns/**").hasRole("CAMPAIGN")
			.antMatchers("/companies/**").hasRole("COMPANY")
			.antMatchers("/devices/**").hasRole("DEVICE")
			.antMatchers("/files/**").hasRole("FILE")
			.antMatchers("/programmings/**").hasRole("PROGRAMMING")
			.antMatchers("/users/**").hasRole("USER")
			.antMatchers("/**").authenticated()
			//.antMatchers("/**").permitAll()
			.and()
			.addFilterBefore(getAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(getValidationTokenFilter(), UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(getCorsFilter(), ChannelProcessingFilter.class)
			.csrf().disable();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(getAuthenticationProvider());
	}

	@Bean
	public DaoAuthenticationProvider getAuthenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(authenticationService);
		authenticationProvider.setSaltSource(getSaltSource());
		authenticationProvider.setPasswordEncoder(getPasswordEncoder());
		return authenticationProvider;
	}
	
	@Bean
	public ReflectionSaltSource getSaltSource() {
		ReflectionSaltSource saltSource = new ReflectionSaltSource();
        saltSource.setUserPropertyToUse("username");
        return saltSource;
	}
	
	@Bean
	public Md5PasswordEncoder getPasswordEncoder() {
		return new Md5PasswordEncoder();
	}
	
    @Bean
	public AuthenticationFilter getAuthenticationFilter() throws Exception{
		return new AuthenticationFilter("/login", authenticationManagerBean()); 
	}
	
	@Bean
	public ValidationTokenFilter getValidationTokenFilter(){
		return new ValidationTokenFilter(); 
	}
	
	@Bean
	public CorsFilter getCorsFilter() {
		return new CorsFilter();
	}
	
}