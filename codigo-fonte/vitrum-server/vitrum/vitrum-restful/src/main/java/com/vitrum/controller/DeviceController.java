package com.vitrum.controller;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitrum.entity.Device;
import com.vitrum.repository.DeviceRepository;
import com.vitrum.service.DeviceService;

/**
 * @author DavidBen
 *
 */
@RestController
@RequestMapping("/devices")
public class DeviceController extends BaseCompanyController<Device, DeviceRepository, DeviceService> {

	@Override
	protected Sort defineSort() {
		return new Sort(Sort.Direction.ASC, "serial");
	}
}