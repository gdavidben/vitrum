package com.vitrum.controller;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitrum.entity.Programming;
import com.vitrum.repository.ProgrammingRepository;
import com.vitrum.service.ProgrammingService;

/**
 * @author DavidBen
 *
 */
@RestController
@RequestMapping("/programmings")
public class ProgrammingController extends BaseCompanyController<Programming, ProgrammingRepository, ProgrammingService>{
	
	@Override
	protected Sort defineSort() {
		return new Sort(Sort.Direction.ASC, "description");
	}
	
}