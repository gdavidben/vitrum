package com.vitrum.controller;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitrum.entity.Campaign;
import com.vitrum.repository.CampaignRepository;
import com.vitrum.service.CampaignService;

/**
 * @author DavidBen
 *
 */
@RestController
@RequestMapping("/campaigns")
public class CampaignController extends BaseCompanyController<Campaign, CampaignRepository, CampaignService> {

	@Override
	protected Sort defineSort() {
		return new Sort(Sort.Direction.ASC, "description");
	}
	
}