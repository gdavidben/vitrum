package com.vitrum.expection.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.vitrum.dto.ExceptionDTO;
import com.vitrum.exception.ResourceAlreadyException;
import com.vitrum.exception.ResourceNotFoundException;

/**
 * @author DavidBen
 *
 */
@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handlerResourceNotFoundException(ResourceNotFoundException e, HttpServletRequest request) {
		ExceptionDTO exceptionDTO = new ExceptionDTO();
		exceptionDTO.setStatus(404L);
		exceptionDTO.setMessage("Probably this resource has not yet been created");
		exceptionDTO.setCause(e.getMessage());
		exceptionDTO.setTimestamp(System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
	}

	@ExceptionHandler(ResourceAlreadyException.class)
	public ResponseEntity<?> handlerResourceAlreadyException(ResourceAlreadyException e, HttpServletRequest request) {
		ExceptionDTO exceptionDTO = new ExceptionDTO();
		exceptionDTO.setStatus(409L);
		exceptionDTO.setMessage("Probably this feature already exists");
		exceptionDTO.setCause(e.getMessage());
		exceptionDTO.setTimestamp(System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
	}

}
