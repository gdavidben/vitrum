package com.vitrum.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.vitrum.dto.FileDTO;
import com.vitrum.entity.Company;
import com.vitrum.service.AuthenticationService;
import com.vitrum.service.FileService;
import com.vitrum.util.ResponseEntityUtil;

/**
 * @author DavidBen
 *
 */
@RestController
@RequestMapping("/files")
public class FileController {

	@Autowired
	@Qualifier(value="getHttpGetCacheDuration")
	protected Long httpGetCacheDuration;
	
	@Autowired
	protected AuthenticationService authenticationService;

	@Autowired
	private FileService service;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAllByKeyword(@RequestParam("keyword") String keyword, @RequestParam("fileType") String fileType, @RequestParam("page") int page, @RequestParam("size") int size, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		List<FileDTO> files = service.findAllByKeyword(keyword, fileType, company);
		return ResponseEntityUtil.ok(files, httpGetCacheDuration);
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<Void> upload(@RequestParam("file") List<MultipartFile> multipartFiles, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);

		service.upload(multipartFiles, company);
		
		return ResponseEntityUtil.noContent();
	}
	
	@RequestMapping(value = "/{fileName:.+}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable String fileName, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		service.delete(fileName, company);
		return ResponseEntityUtil.noContent();
	}
	
}