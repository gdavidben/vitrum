package com.vitrum.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.vitrum.entity.Company;
import com.vitrum.entity.User;
import com.vitrum.repository.UserRepository;
import com.vitrum.service.AuthenticationService;
import com.vitrum.service.UserService;
import com.vitrum.util.ResponseEntityUtil;

/**
 * @author DavidBen
 *
 */
@RestController
@RequestMapping("/users")
public class UserController extends BaseController<User, UserRepository, UserService> {

	@Autowired
	protected AuthenticationService authenticationService;
	
	@Override
	protected Sort defineSort() {
		return new Sort(Sort.Direction.ASC, "username").and(new Sort(Sort.Direction.ASC, "name"));
	}
	
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		User user = service.findByIdAndCompany(id, company);
		return ResponseEntityUtil.ok(user, httpGetCacheDuration);
	}
	
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAllByKeyword(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("keyword") String keyword, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		PageRequest pageRequest = new PageRequest(page, size, defineSort());
		Iterable<User> es = null;
		
		if(keyword != "")
			es = service.findAllByKeywordAndCompany(keyword, company, pageRequest);
		else
			es = service.findAllByCompany(company, pageRequest);
		
		return ResponseEntityUtil.ok(es, httpGetCacheDuration);
	}
	
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		User user = service.findByIdAndCompany(id, company);
		service.delete(user.getId());
		return ResponseEntityUtil.noContent();
	}
	
	@RequestMapping(value = "/restorePassword", method = RequestMethod.PUT)
	public ResponseEntity<?> restorePassword(@RequestBody User user) {
		user = service.findByUsername(user.getUsername());
		service.createPassword(user);
		return ResponseEntityUtil.noContent();
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.PUT)
	public ResponseEntity<?> changePassword(@RequestBody User pUser) {
		String username = getAuthentication().getPrincipal().toString();
		User user = service.findByUsername(username);
		service.changePassword(pUser, user);
		return ResponseEntityUtil.noContent();
	}
}