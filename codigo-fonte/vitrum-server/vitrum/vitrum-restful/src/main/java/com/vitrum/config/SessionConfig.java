/**
 * 
 */
package com.vitrum.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author DavidBen
 *
 */
@Configuration
@PropertySource("classpath:application.properties")
public class SessionConfig {

	@Value("${session.token_duration}")
	private String sessionTokenDuration;
	
	@Bean
	public Long getSessionTokenDuration() {
		return Long.parseLong(sessionTokenDuration);
	}
}
