/**
 * 
 */
package com.vitrum.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author DavidBen
 *
 */
@Configuration
@PropertySource("classpath:application.properties")
public class HttpConfig {

	@Value("${http.get.cache_duration}")
	private String httpGetCacheDuration;
	
	@Bean
	public Long getHttpGetCacheDuration() {
		return Long.parseLong(httpGetCacheDuration);
	}
}
