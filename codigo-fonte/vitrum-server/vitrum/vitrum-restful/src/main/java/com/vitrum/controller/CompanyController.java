package com.vitrum.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.vitrum.entity.Company;
import com.vitrum.repository.CompanyRepository;
import com.vitrum.service.AuthenticationService;
import com.vitrum.service.CompanyService;
import com.vitrum.util.ResponseEntityUtil;

/**
 * @author DavidBen
 *
 */
@RestController
@RequestMapping("/companies")
public class CompanyController extends BaseController<Company, CompanyRepository, CompanyService>{

	@Autowired
	protected AuthenticationService authenticationService;
	
	@Override
	protected Sort defineSort() {
		return new Sort(Sort.Direction.ASC, "cnpj").and(new Sort(Sort.Direction.ASC, "name"));
	}
	
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = service.find(id);
		return ResponseEntityUtil.ok(company, httpGetCacheDuration);
	}
	
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAllByKeyword(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("keyword") String keyword, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		PageRequest pageRequest = new PageRequest(page, size, defineSort());
		Iterable<Company> es = null;
		
		if(keyword != "")
			es = service.findAllByKeyword(keyword, pageRequest);
		else
			es = service.findAll(pageRequest);
		
		return ResponseEntityUtil.ok(es, httpGetCacheDuration);
	}
	
	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = service.find(id);
		service.delete(company.getId());
		return ResponseEntityUtil.noContent();
	}
	
}