package com.vitrum.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.vitrum.entity.BaseCompanyEntity;
import com.vitrum.entity.Company;
import com.vitrum.repository.BaseCompanyRepository;
import com.vitrum.service.AuthenticationService;
import com.vitrum.service.BaseCompanyService;
import com.vitrum.util.ResponseEntityUtil;

/**
 * @author DavidBen
 *
 */
public abstract class BaseCompanyController<E extends BaseCompanyEntity, R extends BaseCompanyRepository<E>, S extends BaseCompanyService<E, R>> extends BaseController<E, R, S> {

	@Autowired
	protected AuthenticationService authenticationService;
	
	@Override
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> create(@Valid @RequestBody E e, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		e.setCompany(company);
		e = service.create(e);
		return ResponseEntityUtil.created("id", e.getId());
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable("id") Long id, @Valid @RequestBody E e, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		e.setId(id);
		e.setCompany(company);
		service.update(e);
		return ResponseEntityUtil.noContent();
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		E entity = service.findByIdAndCompany(id, company);
		service.delete(entity.getId());
		return ResponseEntityUtil.noContent();
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		E e = service.findByIdAndCompany(id, company);
		return ResponseEntityUtil.ok(e, httpGetCacheDuration);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAllByKeyword(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("keyword") String keyword, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		Company company = authenticationService.getCurrentCompany(jsonCompany);
		PageRequest pageRequest = new PageRequest(page, size, defineSort());
		Iterable<E> es = null;
		
		if(keyword != "")
			es = service.findAllByKeywordAndCompany(keyword, company, pageRequest);
		else
			es = service.findAllByCompany(company, pageRequest);
		
		return ResponseEntityUtil.ok(es, httpGetCacheDuration);
	}
	
}
