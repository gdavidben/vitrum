package com.vitrum.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.vitrum.entity.BaseEntity;
import com.vitrum.repository.BaseRepository;
import com.vitrum.service.BaseService;
import com.vitrum.util.ResponseEntityUtil;

/**
 * @author DavidBen
 *
 */
public abstract class BaseController<E extends BaseEntity, R extends BaseRepository<E>, S extends BaseService<E, R>> {

	@Autowired
	@Qualifier(value="getHttpGetCacheDuration")
	protected Long httpGetCacheDuration;
	
	@Autowired
	protected S service;

	protected Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	protected abstract Sort defineSort();
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> create(@Valid @RequestBody E e, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		e = service.create(e);
		return ResponseEntityUtil.created("id", e.getId());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable("id") Long id, @Valid @RequestBody E e, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		e.setId(id);
		service.update(e);
		return ResponseEntityUtil.noContent();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		service.delete(id);
		return ResponseEntityUtil.noContent();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Long id, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		E e = service.find(id);
		return ResponseEntityUtil.ok(e, httpGetCacheDuration);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> findAllByKeyword(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("keyword") String keyword, @CookieValue("Current-Company") String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		PageRequest pageRequest = new PageRequest(page, size, defineSort());
		Iterable<E> es = null;
		
		if(keyword != "")
			es = service.findAllByKeyword(keyword, pageRequest);
		else
			es = service.findAll(pageRequest);
		
		return ResponseEntityUtil.ok(es, httpGetCacheDuration);
	}
}
