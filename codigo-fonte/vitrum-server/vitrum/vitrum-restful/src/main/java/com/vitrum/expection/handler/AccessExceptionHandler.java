/**
 * 
 */
package com.vitrum.expection.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.vitrum.dto.ExceptionDTO;
import com.vitrum.exception.AccessViolationException;

/**
 * @author DavidBen
 *
 */
@ControllerAdvice
public class AccessExceptionHandler {

	@ExceptionHandler(AccessViolationException.class)
	public ResponseEntity<?> handlerAccessViolationException(AccessViolationException e, HttpServletRequest request) {
		ExceptionDTO exceptionDTO = new ExceptionDTO();
		exceptionDTO.setStatus(403L);
		exceptionDTO.setMessage("Unauthorized access");
		exceptionDTO.setCause(e.getMessage());
		exceptionDTO.setTimestamp(System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
	}
}
