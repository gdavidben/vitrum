package com.vitrum.dto;

/**
 * @author DavidBen
 *
 */
public class ExceptionDTO {
	
	private Long status;
	
	private String message;
	
	private String cause;
	
	private String path;
	
	private Long timestamp;

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String caquse) {
		this.cause = caquse;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	
}
