package com.vitrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vitrum.entity.Company;
import com.vitrum.entity.Programming;

/**
 * @author DavidBen
 *
 */
@Repository
public interface ProgrammingRepository extends BaseCompanyRepository<Programming> {

	@Query("SELECT p FROM Programming p WHERE p.description LIKE %?1%")
	Page<Programming> findAllByKeyword(String keyword, Pageable pageable);
	
	@Query("SELECT p FROM Programming p WHERE p.description LIKE %?1% AND p.company = ?2")
	Page<Programming> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable);
}
