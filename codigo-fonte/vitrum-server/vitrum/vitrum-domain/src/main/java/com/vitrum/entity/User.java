package com.vitrum.entity;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * @author DavidBen
 *
 */
@Entity
@Table(name = "vit_user")
@TableGenerator(name = "vit_sequence", table = "vit_sequence", pkColumnName = "id_column", valueColumnName = "next_value", pkColumnValue = "id_user", allocationSize = 1)
public class User extends BaseEntity implements UserDetails {

	private static final long serialVersionUID = 8303206672474917888L;

	@Id
	@Column(name = "id_user", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "vit_sequence")
	private Long id;
	
	@NotEmpty(message = "The field name is not empty")
	@Size(min = 4, max = 80, message = "The length of the field name is invalid")
	private String name;

	@NotEmpty(message = "The field email is not empty")
	@Size(min = 10, max = 80, message = "The length of the field username is invalid")
	private String username;

	//@NotEmpty(message = "The field password is not empty")
	@JsonProperty(access = Access.WRITE_ONLY)
	//@Size(min = 6, max = 80, message = "The length of the field password is invalid")
	private String password;

	@Transient
	@JsonProperty(access = Access.WRITE_ONLY)
	//@Size(min = 6, max = 80, message = "The length of the field newPassword is invalid")
	private String newPassword;
	
	@Transient
	@JsonProperty(access = Access.WRITE_ONLY)
	//@Size(min = 6, max = 80, message = "The length of the field repeatNewPassword is invalid")
	private String repeatNewPassword;
	
	@Column
	private Boolean enabled = Boolean.TRUE;

	@Column(name = "password_temporary")
	private Boolean passwordTemporary = Boolean.TRUE;
	
	@JsonInclude(Include.NON_EMPTY)
	@OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "vit_user_authority", joinColumns = @JoinColumn(name = "id_user", referencedColumnName="id_user"), inverseJoinColumns = @JoinColumn(name = "id_authority", referencedColumnName="id_authority"))
	private Set<Authority> authorities;

	@JsonInclude(Include.NON_EMPTY)
 	@OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "vit_company_user", joinColumns = @JoinColumn(name = "id_user", referencedColumnName="id_user"), inverseJoinColumns = @JoinColumn(name = "id_company", referencedColumnName="id_company"))
	private Set<Company> companies;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRepeatNewPassword() {
		return repeatNewPassword;
	}

	public void setRepeatNewPassword(String repeatNewPassword) {
		this.repeatNewPassword = repeatNewPassword;
	}

	public Boolean getPasswordTemporary() {
		return passwordTemporary;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void encodePassword(Md5PasswordEncoder passwordEncoder, String password) {
		this.password = passwordEncoder.encodePassword(password, this.username);
	}
	
	public Boolean isPasswordTemporary() {
		return passwordTemporary;
	}

	public void setPasswordTemporary(Boolean passwordTemporary) {
		this.passwordTemporary = passwordTemporary;
	}

	@Override
	public Collection<Authority> getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	public Set<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}

}
