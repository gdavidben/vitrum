package com.vitrum.exception;

/**
 * @author DavidBen
 *
 */
public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 289670986822885998L;

	public ResourceNotFoundException(String message) {
		super(message);
	}

	public ResourceNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
