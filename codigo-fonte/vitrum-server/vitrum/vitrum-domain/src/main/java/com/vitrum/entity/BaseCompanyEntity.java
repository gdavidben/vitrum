package com.vitrum.entity;

/**
 * @author DavidBen
 *
 */
public abstract class BaseCompanyEntity extends BaseEntity {

	public abstract void setCompany(Company company);
	
	public abstract Company getCompany();
}
