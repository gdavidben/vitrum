package com.vitrum.dto;

import java.io.Serializable;

/**
 * @author DavidBen
 *
 */
public class FileDTO implements Serializable {

	private static final long serialVersionUID = 1578957203955637367L;

	public static final String TYPE_IMAGE = "IMAGE";
	public static final String TYPE_VIDEO = "VIDEO";
	
	public static final String TYPE_REPOSITORY_SERVER_INTERNAL = "SERVER_INTERNAL";

	private Long id;

	private String name;

	private String typeRepository;

	private String pathRepository;
	
	private String type;

	private Integer size;

	private Boolean enabled = Boolean.TRUE;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeRepository() {
		return typeRepository;
	}

	public void setTypeRepository(String typeRepository) {
		this.typeRepository = typeRepository;
	}

	public String getPathRepository() {
		return pathRepository;
	}

	public void setPathRepository(String pathRepository) {
		this.pathRepository = pathRepository;
	}
	
	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
}
