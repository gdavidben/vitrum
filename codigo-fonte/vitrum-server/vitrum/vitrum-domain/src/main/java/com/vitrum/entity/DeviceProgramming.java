package com.vitrum.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 * @author DavidBen
 *
 */
@Entity
@Table(name = "vit_device_programming")
@TableGenerator(name = "vit_sequence", table = "vit_sequence", pkColumnName = "id_column", valueColumnName = "next_value", pkColumnValue = "id_device_programming", allocationSize = 1)
public class DeviceProgramming extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -3159631518696271808L;

	@Id
	@Column(name = "id_device_programming")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "vit_sequence")
	private Long id;

	@OneToOne
	@JoinColumn(name = "id_device")
	private Device device;

	@OneToOne
	@JoinColumn(name = "id_programming")
	private Programming programming;

	@Column
	private Boolean enabled = Boolean.TRUE;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Programming getProgramming() {
		return programming;
	}

	public void setProgramming(Programming programming) {
		this.programming = programming;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}
