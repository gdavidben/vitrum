package com.vitrum.entity;

/**
 * @author DavidBen
 *
 */
public abstract class BaseEntity {

	public abstract Long getId();

	public abstract void setId(Long id);
}
