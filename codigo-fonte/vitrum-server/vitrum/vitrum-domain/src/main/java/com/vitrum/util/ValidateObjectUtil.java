package com.vitrum.util;

import java.util.ArrayList;

/**
 * @author DavidBen
 *
 */
public class ValidateObjectUtil {

	public static final boolean isStringOK(String string) {
		if(string == null)
			return false;
		else if(string.length() <= 0)
			return false;
		else if(string == "")
			return false;
		else
			return true;
	}
	
	public static final boolean isIterableOK(Iterable<?> arrayList) {
		if(arrayList == null)
			return false;
		else if (arrayList.iterator() == null)
			return false;
		else
			return true;
	}
	
	public static final boolean isArrayListOK(ArrayList<?> arrayList) {
		if(arrayList == null)
			return false;
		else if (arrayList.size() <= 0)
			return false;
		else
			return true;
	}
	
}
