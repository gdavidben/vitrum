package com.vitrum.exception;

/**
 * @author DavidBen
 *
 */
public class ResourceAlreadyException extends RuntimeException {

	private static final long serialVersionUID = 1877043710570669371L;

	public ResourceAlreadyException(String message) {
		super(message);
	}

	public ResourceAlreadyException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
