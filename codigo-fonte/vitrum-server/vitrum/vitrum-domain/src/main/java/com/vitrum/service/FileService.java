package com.vitrum.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.vitrum.dto.FileDTO;
import com.vitrum.entity.Company;

/**
 * @author DavidBen
 *
 */
@Service
public class FileService {

	public static final String IMAGE_TYPE = "IMAGE";
	public static final String VIDEO_TYPE = "VIDEO";
	
	public static final String[] IMAGES_TYPES = { ".png", ".jpg", ".jpeg", ".gif" };
	public static final String[] VIDEOS_TYPES = { ".3gp" };

	public static final String PATH_VITRUM_DATA = Paths.get("").toAbsolutePath().toString() + "\\vitrum-data\\";
	public static final String PATH_VITRUM_DATA_IMAGES = PATH_VITRUM_DATA + "%s\\images\\";
	public static final String PATH_VITRUM_DATA_VIDEOS = PATH_VITRUM_DATA + "%s\\videos\\";
	
	public void upload(List<MultipartFile> multipartFiles, Company company) {
		if (multipartFiles != null && !multipartFiles.isEmpty()) {
			String path = null;

			for (MultipartFile multipartFile : multipartFiles) {
				try {
					String[] originalFileName = multipartFile.getOriginalFilename().split("\\\\");
					String fileName = originalFileName[originalFileName.length - 1];
					String extension = fileName.substring(fileName.indexOf("."));
					java.io.File file = null;

					if (Arrays.asList(IMAGES_TYPES).indexOf(extension) > 0) {
						path = String.format(PATH_VITRUM_DATA_IMAGES, company.getCnpj());
						file = new java.io.File(path + fileName);
					} else if (Arrays.asList(VIDEOS_TYPES).indexOf(extension) > 0) {
						path = String.format(PATH_VITRUM_DATA_VIDEOS, company.getCnpj());
						file = new java.io.File(path + fileName);
					}

					if (file != null) {
						multipartFile.transferTo(file);
					}
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private String definePath(String fileType) {
		switch (fileType) {
		case IMAGE_TYPE:
			return PATH_VITRUM_DATA_IMAGES;
		case VIDEO_TYPE:
			return PATH_VITRUM_DATA_VIDEOS;
		default:
			return null;
		}
	}
	
	public List<FileDTO> findAllByKeyword(String keyword, String fileType, Company company) {
		String path = definePath(fileType);
		path = String.format(path, company.getCnpj());
		
		File folder = new File(path);
		List<FileDTO> files = null;

		if (folder != null && folder.isDirectory()) {
			files = new ArrayList<FileDTO>();

			for (final File file : folder.listFiles()) {
				if (file.getName().contains(keyword)) {
					FileDTO fileDTO = new FileDTO();
					fileDTO.setName(file.getName());
					files.add(fileDTO);
				}
			}
		}
		return files;
	}
	
	public void delete(String fileName, Company company) throws IOException {
		String path = null;
		String extension = fileName.substring(fileName.indexOf("."));

		if (Arrays.asList(IMAGES_TYPES).indexOf(extension) > 0) {
			path = String.format(PATH_VITRUM_DATA_IMAGES, company.getCnpj());
		} else if (Arrays.asList(VIDEOS_TYPES).indexOf(extension) > 0) {
			path = String.format(PATH_VITRUM_DATA_VIDEOS, company.getCnpj());
		}
		
		Files.deleteIfExists(Paths.get(path+fileName));
	}

}
