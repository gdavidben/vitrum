/**
 * 
 */
package com.vitrum.exception;

import org.springframework.security.authentication.InternalAuthenticationServiceException;

/**
 * @author DavidBen
 *
 */
public class AuthenticationException extends InternalAuthenticationServiceException {

	private static final long serialVersionUID = 1230562601176908535L;

	public AuthenticationException(String message) {
		super(message);
	}

	public AuthenticationException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
}
