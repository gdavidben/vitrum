package com.vitrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vitrum.entity.Company;
import com.vitrum.entity.Programming;
import com.vitrum.repository.ProgrammingRepository;

/**
 * @author DavidBen
 *
 */
@Service
public class ProgrammingService extends BaseCompanyService<Programming, ProgrammingRepository>{

	@Override
	protected void beforeCreate(Programming programming) {
		
	}

	@Override
	protected void afterCreate(Programming programming) {
		
	}

	@Override
	protected void beforeUpdate(Programming programming) {
		
	}

	@Override
	protected void afterUpdate(Programming programming) {
		
	}

	@Override
	public Page<Programming> findAllByKeyword(String keyword, Pageable pageable) {
		return repository.findAllByKeyword(keyword, pageable);
	}

	@Override
	public Page<Programming> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable) {
		return repository.findAllByKeywordAndCompany(keyword, company, pageable);
	}
	
}
