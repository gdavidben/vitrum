package com.vitrum.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author DavidBen
 *
 */
@Entity
@Table(name = "vit_company")
@TableGenerator(name = "vit_sequence", table = "vit_sequence", pkColumnName = "id_column", valueColumnName = "next_value", pkColumnValue = "id_company", allocationSize = 1)
public class Company extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 26201827485421399L;

	@Id
	@Column(name = "id_company", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "vit_sequence")
	private Long id;

	@NotEmpty(message = "The field cnpj is not empty")
	@Size(min = 10, max = 80, message = "The length of the field cnpj is invalid")
	private String cnpj;

	@NotEmpty(message = "The field name is not empty")
	@Size(min = 4, max = 80, message = "The length of the field name is invalid")
	private String name;
	
	private Boolean enabled = Boolean.TRUE;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	@Override
	public int hashCode() {
		return this.id.intValue() * this.cnpj.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if(object == null)
			return false;
		if(!(object instanceof Company))
        	return false; 
        if(object == this)
        	return true;
        
        Company company = (Company) object; 
        return this.id.equals(company.getId()) && this.cnpj.equals(company.getCnpj());
	}
}