package com.vitrum.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author DavidBen
 *
 */
@Entity
@Table(name = "vit_device")
@TableGenerator(name = "vit_sequence", table = "vit_sequence", pkColumnName = "id_column", valueColumnName = "next_value", pkColumnValue = "id_device", allocationSize = 1)
public class Device extends BaseCompanyEntity implements Serializable {

	private static final long serialVersionUID = -5720139118826837662L;

	@Id
	@Column(name = "id_device", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "vit_sequence")
	private Long id;

	@OneToOne
	@JoinColumn(name = "id_company")
	private Company company;

	@NotEmpty(message = "The field serial is not empty")
	@Size(max = 20, message = "The length of the field serial is invalid")
	private String serial;

	@Column
	private Boolean enabled = Boolean.TRUE;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Company getCompany() {
		return company;
	}

	@Override
	public void setCompany(Company company) {
		this.company = company;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}
