package com.vitrum.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author DavidBen
 *
 */
@Entity
@Table(name = "vit_authority")
@TableGenerator(name = "vit_sequence", table = "vit_sequence", pkColumnName = "id_column", valueColumnName = "next_value", pkColumnValue = "id_authority", allocationSize = 1)
public class Authority extends BaseEntity implements GrantedAuthority {

	private static final long serialVersionUID = 269026660999297684L;

	@Id
	@Column(name = "id_authority", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "vit_sequence")
	private Long id;

	@Column(nullable = false, unique = true)
	private String authority;

	@NotEmpty(message = "The field description is not empty")
	@Size(max = 100, message = "The length of the field description is invalid")
	private String description;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getAuthority() {
		return this.authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
