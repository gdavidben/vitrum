package com.vitrum.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author DavidBen
 *
 */
@Entity
@Table(name = "vit_campaign")
@TableGenerator(name = "vit_sequence", table = "vit_sequence", pkColumnName = "id_column", valueColumnName = "next_value", pkColumnValue = "id_campaign", allocationSize = 1)
public class Campaign extends BaseCompanyEntity implements Serializable {

	private static final long serialVersionUID = 3067344013230291401L;

	public static final String POSITION_PORTRAIT = "PORTRAIT";
	public static final String POSITION_LANDSPACE = "LANDSPACE";
	public static final String POSITION_BOTH = "BOTH";

	@Id
	@Column(name = "id_campaign", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "vit_sequence")
	private Long id;

	@OneToOne
	@JoinColumn(name = "id_company")
	private Company company;

	@NotEmpty(message = "The field description is not empty")
	@Size(max = 100, message = "The length of the field description is invalid")
	private String description;

	@NotEmpty(message = "The field name is not empty")
	private String position;

	@ManyToMany 
	@JoinTable(name="vit_campaign_files", joinColumns=@JoinColumn(name="id_campaign"), inverseJoinColumns=@JoinColumn(name="id_campaign_file"))
	private Set<CampaignFile> campaignFiles;

	@Column
	private Boolean enabled = Boolean.TRUE;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Company getCompany() {
		return company;
	}

	@Override
	public void setCompany(Company company) {
		this.company = company;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Set<CampaignFile> getCampaignFiles() {
		return campaignFiles;
	}

	public void setCampaignFiles(Set<CampaignFile> campaignFiles) {
		this.campaignFiles = campaignFiles;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}
