package com.vitrum.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author DavidBen
 *
 */
@Entity
@Table(name = "vit_campaign_file")
@TableGenerator(name = "vit_sequence", table = "vit_sequence", pkColumnName = "id_column", valueColumnName = "next_value", pkColumnValue = "id_campaign_file", allocationSize = 1)
public class CampaignFile extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -3159631518696271808L;

	@Id
	@Column(name = "id_campaign_file")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "vit_sequence")
	private Long id;

	@OneToOne
	@JoinColumn(name = "id_campaign")
	private Campaign campaign;

	@NotEmpty(message = "The field path_file is not empty")
	@Column(name = "path_file")
	private String pathFile;

	@NotEmpty(message = "The field duration is not empty")
	private Integer duration;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Campaign getFrame() {
		return campaign;
	}

	public void setFrame(Campaign campaign) {
		this.campaign = campaign;
	}

	public String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

}
