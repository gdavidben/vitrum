package com.vitrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vitrum.entity.Company;
import com.vitrum.entity.Device;
import com.vitrum.repository.DeviceRepository;

/**
 * @author DavidBen
 *
 */
@Service
public class DeviceService extends BaseCompanyService<Device, DeviceRepository>{

	@Override
	protected void beforeCreate(Device device) {
		
	}

	@Override
	protected void afterCreate(Device device) {
		
	}

	@Override
	protected void beforeUpdate(Device device) {
		
	}

	@Override
	protected void afterUpdate(Device device) {
		
	}

	@Override
	public Page<Device> findAllByKeyword(String keyword, Pageable pageable) {
		return repository.findAllByKeyword(keyword, pageable);
	}

	@Override
	public Page<Device> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable) {
		return repository.findAllByKeywordAndCompany(keyword, company, pageable);
	}
	
}
