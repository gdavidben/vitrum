package com.vitrum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vitrum.entity.BaseEntity;
import com.vitrum.repository.BaseRepository;

/**
 * @author DavidBen
 *
 */
@Service
public abstract class BaseService<E extends BaseEntity, R extends BaseRepository<E>> {

	@Autowired
	protected R repository;
	
	protected abstract void beforeCreate(E e);
	protected abstract void afterCreate(E e);
	
	protected abstract void beforeUpdate(E e);
	protected abstract void afterUpdate(E e);
	
	public abstract Page<E> findAllByKeyword(String keyword, Pageable pageable);
	
	public E create(E e) {
		beforeCreate(e);
		
		e.setId(null);
		e = repository.save(e);
		
		afterCreate(e);
		return e;
	}

	public E update(E e) {
		beforeUpdate(e);
		
		if(e != null && exists(e.getId())) {
			e = repository.save(e);
			afterUpdate(e);
		} else {
			e = null;
		}
		return e;
	}

	public void delete(Long id) {
		repository.delete(id);
	}

	public E find(Long id) {
		return repository.findOne(id);
	}
	
	public E find(E e) {
		return repository.findOne(e.getId());
	}

	public Iterable<E> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Iterable<E> findAll() {
		return repository.findAll();
	}
	
	protected Boolean exists(Long id) {
		return repository.exists(id);
	}
	
}
