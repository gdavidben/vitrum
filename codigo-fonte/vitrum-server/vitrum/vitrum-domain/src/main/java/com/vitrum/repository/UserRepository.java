package com.vitrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vitrum.entity.Company;
import com.vitrum.entity.User;

/**
 * @author DavidBen
 *
 */
@Repository
public interface UserRepository extends BaseRepository<User> {

	User findByUsername(String username);
	
	@Query("SELECT u FROM User u WHERE (u.username LIKE %?1% OR u.name LIKE %?1%)")
	Page<User> findAllByKeyword(String keyword, Pageable pageable);
	
	@Query("SELECT u FROM User u WHERE (u.username LIKE %?1% OR u.name LIKE %?1%) AND ?2 MEMBER OF u.companies")
	Page<User> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable);
	
	@Query("SELECT u FROM User u WHERE ?1 MEMBER OF u.companies")
	Page<User> findAllByCompany(Company company, Pageable pageable);
	
	@Query("SELECT u FROM User u WHERE u.id = ?1 AND ?2 MEMBER OF u.companies")
	User findByIdAndCompany(Long id, Company company);
}
