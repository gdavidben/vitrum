package com.vitrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vitrum.entity.Company;
import com.vitrum.entity.Device;

/**
 * @author DavidBen
 *
 */
@Repository
public interface DeviceRepository extends BaseCompanyRepository<Device> {

	@Query("SELECT d FROM Device d WHERE d.serial LIKE %?1%")
	Page<Device> findAllByKeyword(String keyword, Pageable pageable);
	
	@Query("SELECT d FROM Device d WHERE d.serial LIKE %?1% AND d.company = ?2")
	Page<Device> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable);
}
