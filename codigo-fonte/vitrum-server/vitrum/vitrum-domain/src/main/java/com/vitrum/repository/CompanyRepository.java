package com.vitrum.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vitrum.entity.Company;

/**
 * @author DavidBen
 *
 */
@Repository
public interface CompanyRepository extends BaseRepository<Company> {

	Company findByCnpj(String cnpj);
	
	@Query("SELECT c FROM Company c WHERE c.name LIKE %?1% OR c.cnpj LIKE %?1%")
	Page<Company> findAllByKeyword(String keyword, Pageable pageable);
	
	@Query("SELECT c FROM Company c WHERE c.enabled = ?1")
	List<Company> findAllByEnabled(Boolean enabled);
	
	@Query("SELECT c FROM Company c WHERE c.id = ?1")
	Company findById(Long id);
}
