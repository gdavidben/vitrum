package com.vitrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import com.vitrum.entity.BaseCompanyEntity;
import com.vitrum.entity.Company;

/**
 * @author DavidBen
 *
 */
@NoRepositoryBean
public interface BaseCompanyRepository<E extends BaseCompanyEntity> extends BaseRepository<E> {

	E findByIdAndCompany(Long id, Company company);
	
	Page<E> findAllByCompany(Company company, Pageable pageable);
	
	Page<E> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable);
}
