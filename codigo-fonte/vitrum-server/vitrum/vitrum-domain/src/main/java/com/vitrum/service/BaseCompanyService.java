package com.vitrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vitrum.entity.BaseCompanyEntity;
import com.vitrum.entity.Company;
import com.vitrum.repository.BaseCompanyRepository;

/**
 * @author DavidBen
 *
 */
@Service
public abstract class BaseCompanyService<E extends BaseCompanyEntity, R extends BaseCompanyRepository<E>> extends BaseService<E, R> {

	public abstract Page<E> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable);
	
	public E findByIdAndCompany(Long id, Company company) {
		return repository.findByIdAndCompany(id, company);
	}
	
	public E findByIdAndCompany(E e, Company company) {
		return repository.findByIdAndCompany(e.getId(), company);
	}

	public Iterable<E> findAllByCompany(Company company, Pageable pageable) {
		return repository.findAllByCompany(company, pageable);
	}
}
