package com.vitrum.service;

import java.util.UUID;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import com.vitrum.entity.Company;
import com.vitrum.entity.User;
import com.vitrum.exception.ResourceAlreadyException;
import com.vitrum.exception.ResourceNotFoundException;
import com.vitrum.repository.UserRepository;

/**
 * @author DavidBen
 *
 */
@Service
public class UserService extends BaseService<User, UserRepository> {

	@Autowired
	private Md5PasswordEncoder passwordEncoder;
	
	@Autowired
	private JavaMailSenderImpl javaMailSender;

	@Override
	public void beforeCreate(User user) {
		User u = findByUsername(user.getUsername());
		
		if(u != null)
			throw new ResourceAlreadyException("User already");
	}
	
	@Override
	protected void afterCreate(User user) {
		createPassword(user);
	}

	@Override
	public void beforeUpdate(User user) {
		
		//Nas alterações da entidade usuário o campo password sempre virá nulo por questões de segurança.
		//Ao persistir as novas alterações, recuperar o password do usuário.
		if(user.getPassword() == null) {
			User u = find(user.getId());
			user.setPassword(u.getPassword());
		}
	}

	@Override
	public void afterUpdate(User user) {
	
	}

	@Override
	public Page<User> findAllByKeyword(String keyword, Pageable pageable) {
		return repository.findAllByKeyword(keyword, pageable);
	}
	
	public User findByIdAndCompany(Long id, Company company) {
		return repository.findByIdAndCompany(id, company);
	}
	
	public User findByUsername(String username) {
		return repository.findByUsername(username);
	}

	public Page<User> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable) {
		return repository.findAllByKeywordAndCompany(keyword, company, pageable);
	}
	
	public Page<User> findAllByCompany(Company company, Pageable pageable) {
		return repository.findAllByCompany(company, pageable);
	}
	
	public void changePassword(User pUser, User user) {
		if(user == null)
			throw new ResourceNotFoundException("Resource not found");
		
		pUser.setUsername(user.getUsername());
		pUser.encodePassword(passwordEncoder, pUser.getPassword());
		
		if(!user.getPassword().equals(pUser.getPassword()))
			throw new ValidationException("Incorrect password");

		if(!pUser.getNewPassword().equals(pUser.getRepeatNewPassword()))
			throw new ValidationException("Incorrect new password");
		
		user.encodePassword(passwordEncoder, pUser.getNewPassword());
		
		if(user.getPasswordTemporary())
			user.setPasswordTemporary(Boolean.FALSE);
		
		update(user);
	}
	
	public void createPassword(User user) {
		String password = UUID.randomUUID().toString().substring(0, 10);
		user.encodePassword(passwordEncoder, password);
		user.setPasswordTemporary(Boolean.TRUE);
		user = update(user);
		
		sendEmailCreatePassword(user, password);
	}

	private Boolean sendEmailCreatePassword(User user, String password) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		StringBuilder content = new StringBuilder();
		
		content.append("Prezado(a), " + user.getName() + ".");
		content.append(System.getProperty("line.separator")).append(System.getProperty("line.separator"));
		content.append("A senha [ "+ password +" ] foi gerada automaticamente para a sua conta.");
		content.append(System.getProperty("line.separator")).append(System.getProperty("line.separator"));
		content.append("No próximo acesso ao sistema, será necessário redefinir uma nova senha.");
		content.append(System.getProperty("line.separator")).append(System.getProperty("line.separator"));
		content.append("Lembre-se dos seguintes critérios de segurança ao redefiní-la: ");
		content.append(System.getProperty("line.separator"));
		content.append("- Ter no mínimo 6 caracteres;");
//		content.append(System.getProperty("line.separator"));
//		content.append("- Conter letras maiúsculas e minúsculas;");
//		content.append(System.getProperty("line.separator"));
//		content.append("- Conter números.");
		content.append(System.getProperty("line.separator")).append(System.getProperty("line.separator"));
		content.append("Atenciosamente,");
		content.append(System.getProperty("line.separator"));
		content.append("Equipe Vitrum.");
		
		mailMessage.setTo(user.getUsername());
		mailMessage.setSubject("Dados de acesso");
		mailMessage.setText(content.toString());
		
		javaMailSender.send(mailMessage);
		
		return Boolean.TRUE;
	}

}
