package com.vitrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vitrum.entity.Campaign;
import com.vitrum.entity.Company;
import com.vitrum.repository.CampaignRepository;

/**
 * @author DavidBen
 *
 */
@Service
public class CampaignService extends BaseCompanyService<Campaign, CampaignRepository>{

	@Override
	protected void beforeCreate(Campaign campaign) {
		
	}

	@Override
	protected void afterCreate(Campaign campaign) {
		
	}

	@Override
	protected void beforeUpdate(Campaign campaign) {
		
	}

	@Override
	protected void afterUpdate(Campaign campaign) {
		
	}

	@Override
	public Page<Campaign> findAllByKeyword(String keyword, Pageable pageable) {
		return repository.findAllByKeyword(keyword, pageable);
	}

	@Override
	public Page<Campaign> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable) {
		return repository.findAllByKeywordAndCompany(keyword, company, pageable);
	}

}
