/**
 * 
 */
package com.vitrum.exception;

/**
 * @author DavidBen
 *
 */
public class AccessViolationException extends RuntimeException {

	private static final long serialVersionUID = -3073464464932509105L;

	public AccessViolationException(String message) {
		super(message);
	}

	public AccessViolationException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
