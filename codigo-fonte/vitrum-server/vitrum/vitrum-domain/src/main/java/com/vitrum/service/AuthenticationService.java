package com.vitrum.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitrum.entity.Company;
import com.vitrum.entity.User;
import com.vitrum.exception.AccessViolationException;
import com.vitrum.repository.CompanyRepository;
import com.vitrum.repository.UserRepository;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author DavidBen
 *
 */
@Service
public class AuthenticationService implements UserDetailsService {
	
	private static Logger logger = LoggerFactory.getLogger(AuthenticationService.class);
	
	private static final String AUTH_TOKEN = "Auth-Token";
	private static final String CURRENT_CNPJ = "Current-Company";
	
	private final String secret;
	
	@Autowired
	@Qualifier(value="getSessionTokenDuration")
	private Long sessionTokenDuration;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	public AuthenticationService() {
		super();
		secret = UUID.randomUUID().toString();
	}

	private String createAuthToken(String username) {
		Date now = new Date();
		Date expiration = new Date(now.getTime() + TimeUnit.SECONDS.toMillis(sessionTokenDuration));
		
		return Jwts.builder()
				.setId(UUID.randomUUID().toString())
				.setSubject(username)
				.setIssuedAt(now)
				.setExpiration(expiration)
				.signWith(SignatureAlgorithm.HS512, secret)
				.compact();
	}
	
	public void addAuthHeaderResponse(HttpServletResponse response, Authentication authentication) {
		User user = (User) authentication.getPrincipal();
		String token = createAuthToken(user.getUsername());
		logger.info("Usuario " + user.getUsername() + " autenticado.");
		createAuthHeader(response, token);
	}
	
	private Claims parseTokenFromClaims(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}
	
	private User parseTokenFromUser(String token) {
		Claims claims = parseTokenFromClaims(token);
		String username = claims.getSubject();
		return (User) loadUserByUsername(username);
	}
	
	private void createAuthHeader(HttpServletResponse response, String token) {
		response.setHeader(AUTH_TOKEN, Base64Utils.encodeToString(token.getBytes()));
	}
	
	private String validateToken(String token) {
		Claims claims = parseTokenFromClaims(token);
		return createAuthToken(claims.getSubject());
	}
	
	public UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		Cookie cookie = getCookie(request, AUTH_TOKEN);
		if (cookie != null) {
			if(cookie.getValue() != "null") {
				String token = URLDecoder.decode(cookie.getValue(), "UTF-8");
				token = new String(Base64Utils.decodeFromString(token));
		
				token = validateToken(token);
				if (token != null) {
					final User user = parseTokenFromUser(token);
					if (user != null) {
						createAuthHeader(response, token);
						logger.info("Usuario " + user.getUsername() + " renovou seu token.");
						return new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities());
					}
				}
			}
		}
		return null;
	}
	
	public void validateAccessCompany(ServletRequest request) throws JsonParseException, JsonMappingException, IOException {
		User user = getCurrentUser();
		Company company = getCurrentCompany(request);
		
		if(user != null && company != null && !user.getCompanies().contains(company))
			throw new AccessViolationException("Unauthorized access");
	}
	
	public User getCurrentUser() {
		User user = null;
		
		if(SecurityContextHolder.getContext().getAuthentication() != null) {
			if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
				user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			}else if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof Object) {
				user = new User();
				user.setUsername(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
			}
			user = userRepository.findByUsername(user.getUsername());
		}
		
		return user;
	}
	
	private Company getCurrentCompany(ServletRequest request) throws JsonParseException, JsonMappingException, IOException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Cookie cookie = getCookie(httpRequest, CURRENT_CNPJ);
		String jsonCompany = URLDecoder.decode(cookie.getValue(), "UTF-8"); 
		Company company = getCurrentCompany(jsonCompany);
		return company;
	}
	
	public Company getCurrentCompany(String jsonCompany) throws JsonParseException, JsonMappingException, IOException {
		jsonCompany = URLDecoder.decode(jsonCompany, "UTF-8"); 
		jsonCompany = new String(Base64Utils.decodeFromString(jsonCompany));
		Company company = new ObjectMapper().readValue(jsonCompany, Company.class);
		company = companyRepository.findOne(company.getId());
		return company;
	}
	
	public Company getCurrentCompanyValidate(ServletRequest request) throws JsonParseException, JsonMappingException, IOException {
		Company company = getCurrentCompany(request);
		User user = getCurrentUser();

		if (user.getCompanies().contains(company)) {
			return company;
		} else {
			return null;
		}
	}
	
	private Cookie getCookie(HttpServletRequest request, String cookieName) {
		Cookie returnCookie = null;
		Cookie[] cookies = request.getCookies();
		
		if(cookies != null && cookies.length > 0) {
			for (Cookie cookie : cookies) {
				if(cookie.getName().equals(cookieName)) {
					returnCookie = cookie;
					break;
				}
			}
		}
		return returnCookie;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findByUsername(username);
	}
}
