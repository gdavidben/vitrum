package com.vitrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vitrum.entity.Campaign;
import com.vitrum.entity.Company;

/**
 * @author DavidBen
 *
 */
@Repository
public interface CampaignRepository extends BaseCompanyRepository<Campaign> {

	@Query("SELECT c FROM Campaign c WHERE c.description LIKE %?1%")
	Page<Campaign> findAllByKeyword(String keyword, Pageable pageable);
	
	@Query("SELECT c FROM Campaign c WHERE c.description LIKE %?1% AND c.company = ?2")
	Page<Campaign> findAllByKeywordAndCompany(String keyword, Company company, Pageable pageable);
}
