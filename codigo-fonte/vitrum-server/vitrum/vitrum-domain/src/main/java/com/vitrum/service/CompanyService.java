package com.vitrum.service;

import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.vitrum.entity.Company;
import com.vitrum.exception.ResourceAlreadyException;
import com.vitrum.repository.CompanyRepository;
import com.vitrum.util.ValidateObjectUtil;

/**
 * @author DavidBen
 *
 */
@Service
public class CompanyService extends BaseService<Company, CompanyRepository>{

	public CompanyService() {
	}
	
	@PostConstruct
	public void loadDirs() {
		String path = Paths.get("").toAbsolutePath().toString() + "\\vitrum-data";
		String pathCompany = null;
		java.io.File folderCompany = null;
		java.io.File folderRoot = new java.io.File(path);
		
		if(!folderRoot.exists()) {
			folderRoot.mkdirs();
		}
		
		Iterable<Company> companies = findAllByEnabled(Boolean.TRUE);
		
		if(ValidateObjectUtil.isIterableOK(companies)) {
			for (Company company : companies) {
				pathCompany = path + "\\" + company.getCnpj();
				folderCompany = new java.io.File(pathCompany);
				
				if(!folderCompany.exists()) {
					folderCompany.mkdirs();
					folderCompany = new java.io.File(pathCompany + "\\images");
					folderCompany.mkdirs();
					folderCompany = new java.io.File(pathCompany + "\\videos");
					folderCompany.mkdirs();
				}
			}
		}
	}
	
	@Override
	protected void beforeCreate(Company company) {
		Company c = findByCnpj(company.getCnpj());
		
		if(c != null)
			throw new ResourceAlreadyException("Company already");
	}

	@Override
	protected void afterCreate(Company company) {
		
	}

	@Override
	protected void beforeUpdate(Company company) {
		
	}

	@Override
	protected void afterUpdate(Company company) {
		
	}

	@Override
	public Page<Company> findAllByKeyword(String keyword, Pageable pageable) {
		return repository.findAllByKeyword(keyword, pageable);
	}
	
	public Company findByCnpj(String cnpj) {
		return repository.findByCnpj(cnpj);
	}
	
	public Iterable<Company> findAllByEnabled(Boolean enabled) {
		return repository.findAllByEnabled(enabled);
	}

}
